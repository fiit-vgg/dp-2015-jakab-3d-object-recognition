//    File: imageviewer.h
//
//	  Date: February 11, 2013
//
//  Author: Marek Jakab, Marek Racev

#ifndef IMAGEVIEWER_H
#define IMAGEVIEWER_H

#include <QtWidgets\QDialog>
#include <QtWidgets\QStatusBar>
#include <QtWidgets\QGraphicsScene>
#include <QtCore\QTime>
#include <QMutex>

namespace Ui {
	class ImageViewer;
}

class ImageViewer : public QDialog
{
	Q_OBJECT

public:
	explicit ImageViewer(QWidget *parent = 0);
	~ImageViewer();

	void showImage(QImage &image);
	void setScaleFactor(double scaleFactor);
	void setText(const QString &text);

private:
	void showText(QImage &image);
	void showFps();
	void delay(int miliseconds);

private:
	Ui::ImageViewer *ui;
	QGraphicsScene *scene;
	
	QStatusBar *statusBar;
	QTime time, initialTime;
	int frames, totalFrames;
	QMutex mutex;

	QString text;
};

#endif // IMAGEVIEWER_H
