//    File: mainwindow.cpp
//
//	  Date: April, 2015
//
//  Author: Marek Jakab, Marek Racev


#include "mainwindow.h"
#include "ui_mainwindow.h"


/*!
	\class MainWindow
	\brief Main window.
*/


/*!
	Constructs a MainWindow object.
*/
MainWindow::MainWindow(QWidget *parent) :
	QMainWindow(parent),
	ui(new Ui::MainWindow)
{
	ui->setupUi(this);

	// Menu
	QObject::connect(ui->actionOpenFile, SIGNAL(triggered()), this, SLOT(openRecognitionConfigurationFile()));

	// UI
	QObject::connect(ui->initializePushButton, SIGNAL(clicked()), this, SLOT(initializeSensor()));
	QObject::connect(ui->shutdownPushButton, SIGNAL(clicked()), this, SLOT(shutdownSensor()));
	
	QObject::connect(ui->trainPushButton, SIGNAL(clicked()), this, SLOT(trainObject()));

	// Kinect
	qRegisterMetaType<HRESULT>("HRESULT");
	QObject::connect(this, SIGNAL(statusChanged(HRESULT)), this, SLOT(displayStatus(HRESULT)), Qt::QueuedConnection);
	this->m_sensor = NULL;
	
	onSensorShutdowned();

	colorStreamWindow = NULL;
	depthStreamWindow = NULL;
	alignedDepthStreamWindow = NULL;
	colorSegmentWindow = NULL;

	this->move(0, QApplication::desktop()->height() - 250);
	this->resize(0, 0);
}


/*!
	Destroys a MainWindow object.
*/
MainWindow::~MainWindow()
{
	delete ui;
}


/*!
	Processes close \a event.
*/
void MainWindow::closeEvent(QCloseEvent *event)
{
	shutdownSensor();
	event->accept();
}


/*!
	Processes timer \a event.
*/
void MainWindow::timerEvent(QTimerEvent *event)
{
	if (!timer.isActive())
		return;
	//// acquire images
	cv::Mat colorImage2, alignedDepthImage2, depthImage2, alignedColorImage2, depthImageVis2;
	HRESULT hr = m_sensor->acquireImages(depthImage2, colorImage2);
	if (!SUCCEEDED(hr))
	{
		return;
	}

	cv::Mat depth_frame_visual;
	m_sensor->visualiseDepthMap(depthImage2, depth_frame_visual);
	thread.recognize(colorImage2, depthImage2);

	// show images
	colorStreamWindow->showImage(Mat2QImage(colorImage2));
	depthStreamWindow->showImage(Mat2QImage(depth_frame_visual));

}

/*!
	Create QImage from cv::Mat in Qt5
*/
QImage MainWindow::Mat2QImage(cv::Mat const& src)
{
	cv::Mat temp;
	cvtColor(src, temp, CV_BGR2RGB);
	QImage dest((const uchar *)temp.data, temp.cols, temp.rows, temp.step, QImage::Format_RGB888);
	dest.bits(); // enforce deep copy
	temp.release();
	return dest;
}

/*!
	Redraws image widget with new color \a image.
*/
void MainWindow::updatePixmap(const cv::Mat &image)
{
	if (colorSegmentWindow != NULL)
		colorSegmentWindow->showImage(Mat2QImage(image));
		//colorSegmentWindow->showImage(QImage((uchar *)image.data, image.cols, image.rows, image.step, QImage::Format_RGB32));
}


/*!
	Redraws image widget with new depth \a image.
*/
void MainWindow::updateDepth(const cv::Mat &image)
{
	if (alignedDepthStreamWindow == NULL)
		return;
	cv::Mat vis_frame;
	m_sensor->visualiseDepthMap(image, vis_frame);
	alignedDepthStreamWindow->showImage(Mat2QImage(vis_frame));
	vis_frame.release();
}


/*!
	Redraws image viewer with new \a image and \a title.
*/
void MainWindow::updateImage(const cv::Mat &image, const QString &title)
{
	cv::imshow(title.toStdString(), image);
}


/*!
	Initializes Kinect sensor and starts object recognition.
*/
void MainWindow::initializeSensor()
{

	if (m_sensor == NULL)
		m_sensor = kcv::KCV_sensor::getInstance();
	m_sensor->initSensor();

	onSensorInitialized();

	if (!(colorStreamOpen() && depthStreamOpen() && alignedDepthStreamOpen())) {
		shutdownSensor();
		return;
	}		

	qRegisterMetaType<cv::Mat>("cv::Mat");

	QObject::connect(&thread, SIGNAL(depthImageReady(const cv::Mat &)), this, SLOT(updateDepth(const cv::Mat &)), Qt::QueuedConnection);
	QObject::connect(&thread, SIGNAL(segmentedImageReady(const cv::Mat &)), this, SLOT(updatePixmap(const cv::Mat &)), Qt::QueuedConnection);
	QObject::connect(&thread, SIGNAL(imageReady(const cv::Mat &, const QString &)), this, SLOT(updateImage(const cv::Mat &, const QString &)), Qt::QueuedConnection);
	QObject::connect(&thread, SIGNAL(objectRecognized(const QString &)), this, SLOT(onObjectRecognition(const QString &)), Qt::QueuedConnection);

	QObject::connect(&thread, SIGNAL(configurationFileNotFound(const QString &)), this, SLOT(showConfigurationFileNotFoundMessage(const QString &)), Qt::QueuedConnection);
	QObject::connect(&thread, SIGNAL(configurationFileErrorStatus(const QString &, int)), this, SLOT(showConfigurationFileErrorStatusMessage(const QString &, int)), Qt::QueuedConnection);
	QObject::connect(&thread, SIGNAL(unsupportedRecognizerType(const QString &)), this, SLOT(showUnsupportedRecognizerMessage(const QString &)), Qt::QueuedConnection);
	QObject::connect(&thread, SIGNAL(unsupportedDetectorType(const QString &)), this, SLOT(showUnsupportedDetectorMessage(const QString &)), Qt::QueuedConnection);
	QObject::connect(&thread, SIGNAL(unsupportedDescriptorType(const QString &)), this, SLOT(showUnsupportedDescriptorMessage(const QString &)), Qt::QueuedConnection);
	QObject::connect(&thread, SIGNAL(unsupportedMatcherType(const QString &)), this, SLOT(showUnsupportedMatcherMessage(const QString &)), Qt::QueuedConnection);
	QObject::connect(&thread, SIGNAL(imageNotFound(const QString &)), this, SLOT(showImageNotFoundMessage(const QString &)), Qt::QueuedConnection);
	QObject::connect(&thread, SIGNAL(unsupportedGpu()), this, SLOT(showUnsupportedGpuMessage()), Qt::QueuedConnection);


	if (!thread.loadSettings())
	{
		shutdownSensor();
		return;
	}
		
	timer.start(timerInterval, this);
}


/*!
	Stops object recognition and shuts down Kinect sensor.
*/
void MainWindow::shutdownSensor()
{
	QObject::disconnect(&thread, 0, 0, 0);
	timer.stop();

	delete colorSegmentWindow;
	colorSegmentWindow = NULL;
	delete alignedDepthStreamWindow;
	alignedDepthStreamWindow = NULL;
	delete depthStreamWindow;
	depthStreamWindow = NULL;
	delete colorStreamWindow;
	colorStreamWindow = NULL;

	if (thread.isRunning())
		thread.stop();

	if (this->m_sensor != NULL)
	{
		this->m_sensor->closeAll();
		this->m_sensor = NULL;
	}

	onSensorShutdowned();
}

/*!
	Opens color stream and image viewers.
*/
bool MainWindow::colorStreamOpen()
{
	if (!m_sensor->isAvailable())
	{
		return false;
	}
	
	if (colorStreamWindow == NULL) {
		colorStreamWindow = new ImageViewer();
		colorStreamWindow->setWindowTitle(tr("Color Stream"));
		colorStreamWindow->move(0, 0);
		colorStreamWindow->show();
	}
	if (colorSegmentWindow == NULL) {
		colorSegmentWindow = new ImageViewer();
		colorSegmentWindow->setWindowTitle(tr("Color Segment"));
		colorSegmentWindow->move(0, 0);
		colorSegmentWindow->show();
	}

	return true;
}


/*!
	Opens both depth stream and aligned depth stream.
*/
void MainWindow::depthStreamsOpen()
{
	depthStreamOpen();
	alignedDepthStreamOpen();
}


/*!
	Opens depth stream and image viewers.
*/
bool MainWindow::depthStreamOpen()
{
	if (!m_sensor->isAvailable())
		return false;

	if (depthStreamWindow == NULL) {
		depthStreamWindow = new ImageViewer();
		depthStreamWindow->setWindowTitle(tr("Depth Stream"));
		depthStreamWindow->move(655, 0);
		depthStreamWindow->show();
	}

	return true;
}


/*!
	Opens aligned depth stream.
*/
bool MainWindow::alignedDepthStreamOpen()
{
	if (!m_sensor->isAvailable())
		return false;

	if (alignedDepthStreamWindow == NULL) {
		alignedDepthStreamWindow = new ImageViewer();
		alignedDepthStreamWindow->setWindowTitle(tr("Aligned Depth Stream"));
		alignedDepthStreamWindow->move(655, 0);
		alignedDepthStreamWindow->show();
	}

	return true;
}

/*!
	Disables and enables various GUI elements after sensor initialization.
*/
void MainWindow::onSensorInitialized()
{
	ui->initializePushButton->hide();
	ui->shutdownPushButton->show();
	ui->trainPushButton->setEnabled(true);
	ui->actionOpenFile->setDisabled(true);
}


/*!
	Disables and enables various GUI elements after sensor shutdown.
*/
void MainWindow::onSensorShutdowned()
{
	ui->shutdownPushButton->hide();
	ui->initializePushButton->show();
	ui->trainPushButton->setDisabled(true);
	ui->actionOpenFile->setEnabled(true);
}


/*!
	Shuts down Kinect sensor and displays error \a message.
*/
void MainWindow::showErrorMessage(const QString &message)
{
	shutdownSensor();
	QMessageBox::critical(this, tr("Object Recognizer"), message);
}


void MainWindow::showConfigurationFileErrorStatusMessage(const QString &fileName, int status)
{	
	switch (status) {
		case QSettings::AccessError: showErrorMessage(tr("Configuration file %1: An access error occurred.").arg(fileName)); break;
		case QSettings::FormatError: showErrorMessage(tr("Configuration file %1: A format error occurred.").arg(fileName)); break;
	}
}


void MainWindow::showConfigurationFileNotFoundMessage(const QString &fileName)
{
	if (fileName.isEmpty())
		showErrorMessage(tr("Please, open a configuration file."));
	else
		showErrorMessage(tr("Configuration file %1 not found.").arg(fileName));
}


void MainWindow::showUnsupportedRecognizerMessage(const QString &type) { showErrorMessage(tr("Can not create recognizer of %1 type.").arg(type)); }
void MainWindow::showUnsupportedDetectorMessage(const QString &type) { showErrorMessage(tr("Can not create detector of %1 type.").arg(type)); }
void MainWindow::showUnsupportedDescriptorMessage(const QString &type) { showErrorMessage(tr("Can not create descriptor of %1 type.").arg(type)); }
void MainWindow::showUnsupportedMatcherMessage(const QString &type) { showErrorMessage(tr("Can not create matcher of %1 type.").arg(type)); }
void MainWindow::showImageNotFoundMessage(const QString &fileName) { showErrorMessage(tr("Image file %1 not found.").arg(fileName)); }
void MainWindow::showUnsupportedGpuMessage() { showErrorMessage(tr("GPU unsupported.")); }


/*!
	Opens recognition configuration file.
*/
void MainWindow::openRecognitionConfigurationFile()
{
	QString fileName = QFileDialog::getOpenFileName(this, tr("Open Recognition Configuration File"), QDir::currentPath(), tr("INI Files (*.ini)"));
	if (fileName.isEmpty())
		return;
	
	this->fileName = fileName;
	thread.setConfigFile(fileName);
}


/*!
	Processes object recognition event.
*/
void MainWindow::onObjectRecognition(const QString &name)
{
	if (colorSegmentWindow != NULL)
		colorSegmentWindow->setText(name);
}


/*!
	Trains current captured object.
*/
void MainWindow::trainObject()
{
	timer.stop();

	QInputDialog* inputDialog = new QInputDialog(this);
	bool accepted = false;
	QString text = inputDialog->getText(this, "Train Object",
		"Object name:", QLineEdit::Normal,
		QDir::home().dirName(), &accepted);
	if (accepted) {
		QFileInfo fileInfo(this->fileName); //configurationFileName
		QString fileName = QFileDialog::getSaveFileName(this, tr("Save Object"), fileInfo.path());
		if (fileName.isEmpty())
			fileName = fileInfo.path();
		thread.trainObject(text, fileName);
	}

	timer.start(timerInterval, this);
}

void MainWindow::delay(int miliseconds)
{
	QTime dieTime = QTime::currentTime().addMSecs(miliseconds);
	while (QTime::currentTime() < dieTime)
		QCoreApplication::processEvents(QEventLoop::AllEvents, 100);
}