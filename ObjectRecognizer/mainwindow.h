//    File: mainwindow.h
//
//	  Date: April, 2015
//
//  Author: Marek Jakab, Marek Racev

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QtWidgets/QApplication>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QDesktopWidget>
#include <QtWidgets/QFileDialog>
#include <QtWidgets/QMessageBox>
#include <QSettings>
#include <QBasicTimer>
#include <QtGui>

#include <QtWidgets/QInputDialog>

#include <imageviewer.h>

#include <recognizethread.h>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow
{
	Q_OBJECT

public:
	explicit MainWindow(QWidget *parent = 0);
	~MainWindow();

signals:
	void statusChanged(HRESULT status);

protected:
	void closeEvent(QCloseEvent *event);
	void timerEvent(QTimerEvent *event);

private slots:
	void initializeSensor();
	void shutdownSensor();
	bool colorStreamOpen();
	void depthStreamsOpen();
	void updatePixmap(const cv::Mat &image);
	void updateDepth(const cv::Mat &image);
	void updateImage(const cv::Mat &image, const QString &title);
	void onSensorInitialized();
	void onSensorShutdowned();

	void showErrorMessage(const QString &message);
	void showConfigurationFileNotFoundMessage(const QString &fileName);
	void showConfigurationFileErrorStatusMessage(const QString &fileName, int status);
	void showUnsupportedRecognizerMessage(const QString &type);
	void showUnsupportedDetectorMessage(const QString &type);
	void showUnsupportedDescriptorMessage(const QString &type);
	void showUnsupportedMatcherMessage(const QString &type);
	void showImageNotFoundMessage(const QString &fileName);
	void showUnsupportedGpuMessage();

	void openRecognitionConfigurationFile();
	void onObjectRecognition(const QString &name);
	void trainObject();

private:
	bool depthStreamOpen();
	bool alignedDepthStreamOpen();
	void delay(int miliseconds);
	//static void CALLBACK statusProcessCallback(HRESULT status, const OLECHAR *instanceName, const OLECHAR *uniqueDeviceName, void *userData);
	//void CALLBACK statusProcess(HRESULT status, const OLECHAR *instanceName, const OLECHAR *uniqueDeviceName);

	//void updateSensorComboBox() const;
	QImage Mat2QImage(cv::Mat const& src);

private:
	Ui::MainWindow *ui;
	
	QString fileName;
	RecognizeThread thread;

	kcv::KCV_sensor *m_sensor;

	//KinectX sensor;
	
	ImageViewer *colorStreamWindow;
	ImageViewer *depthStreamWindow;
	ImageViewer *alignedDepthStreamWindow;
	ImageViewer *colorSegmentWindow;

	QBasicTimer timer;
	static const int timerInterval = 20;
};

#endif // MAINWINDOW_H
