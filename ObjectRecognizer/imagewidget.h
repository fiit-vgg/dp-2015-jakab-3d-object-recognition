//    File: imagewidget.h
//
//	  Date: February 9, 2013
//
//  Author: Marek Racev

#ifndef IMAGEWIDGET_H
#define IMAGEWIDGET_H

#include <QtWidgets\QGraphicsView>

class ImageWidget : public QGraphicsView
{
	Q_OBJECT

public:
	ImageWidget(QWidget *parent = 0);
	~ImageWidget();

public:
	void zoom(qreal scaleFactor);
	
protected:
	void keyPressEvent(QKeyEvent *event);
	void mouseDoubleClickEvent(QMouseEvent *event);
	void wheelEvent(QWheelEvent *event);
};

#endif // IMAGEWIDGET_H
