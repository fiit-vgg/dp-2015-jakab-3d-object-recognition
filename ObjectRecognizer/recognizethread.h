//    File: recognizethread.h
//
//	  Date: April, 2015
//
//  Author: Marek Jakab, Marek Racev

#ifndef RECOGNIZETHREAD_H
#define RECOGNIZETHREAD_H

#include <QThread>
#include <QSettings>
#include <QtWidgets/QFileDialog>
#include <QtWidgets/QMessageBox>

#include <QHash>
#include <QMutex>
#include <QWaitCondition>

#include <Kinect2X.h>
#include <ObjectRecognizer.h>

#include <opencv2/nonfree/nonfree.hpp>
#include <opencv2/nonfree/features2d.hpp>

#include <QDebug>
#include <QFileInfo>
#include <QMetaType>
#include <QSettings>
#include <QStringList>

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/nonfree/features2d.hpp>
#include <opencv2/nonfree/nonfree.hpp>
#include <SiftGPU.h>

class RecognizeThread : public QThread
{
	Q_OBJECT

	struct RecognizableObject
	{
		int id;
		int view;
		QString name;
	};

	typedef enum { BLR_NONE, BLR_MEDIAN_BLUR, BLR_DILATATION, BLR_CLOSING } BLACK_LINES_REMOVAL_METHOD;

public:
	RecognizeThread(QObject *parent = 0);
	~RecognizeThread();
	
private:
	RecognizeThread(const RecognizeThread &) {}
	RecognizeThread &operator=(const RecognizeThread &) {}

public:
	//void configureRecognition(const QString &configurationFileName);
	void setConfigFile(QString fileName);
	bool loadSettings();
	void recognize(const cv::Mat &colorImage, const cv::Mat &depthImage);
	void trainObject(const QString &objectName, const QString &fileName);
	void stop();
	
signals:
	void segmentedImageReady(const cv::Mat &image);
	void depthImageReady(const cv::Mat &image);
	void imageReady(const cv::Mat &image, const QString &title);
	void objectRecognized(const QString &name);

	void configurationFileNotFound(const QString &fileName);
	void configurationFileErrorStatus(const QString &fileName, int status);
	void unsupportedRecognizerType(const QString &type);
	void unsupportedDetectorType(const QString &type);
	void unsupportedDescriptorType(const QString &type);
	void unsupportedMatcherType(const QString &type);
	void imageNotFound(const QString &fileName);
	void unsupportedGpu(void);
	
private:
	void run();
	void runRecognition();
	
private:
	//bool initialize();
	void display(const cv::Mat &image, QString &title, bool binary = false);

	static void setParameter(cv::Algorithm *algorithm, const std::string &parameter, const QVariant &value);
	static void removeBlackLines(const cv::Mat &src, cv::Mat &dst, BLACK_LINES_REMOVAL_METHOD method);
	static int floodFillSegmentation(const cv::Mat &depthImage, cv::Mat &mask);
	static int findSeedPoint(const cv::Mat &depthImage, cv::Point &seed);

private:
	QMutex mutex;
	QWaitCondition condition;

	bool isReady;
	bool abort;

	QString configurationFileName;

	cv::Mat colorImage;
	cv::Mat depthImage;

	cv::Mat maskedColorImage;
	cv::Mat mask;

	std::vector<RecognizableObject> objectList;
	cv::Ptr<ObjectRecognizer> recognizer;
	kcv::KCV_sensor *kcv_sensor;

	QHash<QString, cv::Mat> images;

	//kcv::KCV_sensor *kcv;

	int cWidth;
	int cHeight;
	int dWidth;
	int dHeight;
};

#endif // RECOGNIZETHREAD_H
