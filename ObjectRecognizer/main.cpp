#include "mainwindow.h"
#include <QtWidgets\QApplication>

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	QCoreApplication::setOrganizationName("FIIT");
	QCoreApplication::setOrganizationDomain("fiit.stuba.sk");
	QCoreApplication::setApplicationName("Object Recognizer");
	QSettings::setDefaultFormat(QSettings::IniFormat);
	MainWindow w;
	w.show();

	return a.exec();
}
