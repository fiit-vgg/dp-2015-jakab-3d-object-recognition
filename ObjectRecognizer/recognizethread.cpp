//    File: recognizethread.cpp
//
//	  Date: April, 2015
//
//  Author: Marek Jakab, Marek Racev


#include "recognizethread.h"

#include <iostream>
#include <fstream>

const cv::Scalar WHITE(255, 255, 255);
const cv::Scalar BLACK(  0,   0,   0);


/*!
	\class RecognizeThread
	\brief The RecognizeThread class manages object recognition in a separate thread using RGB-D information from Kinect sensor.
*/


/*!
	Constructs a RecognizeThread object.
*/
RecognizeThread::RecognizeThread(QObject *parent)
	: QThread(parent)
{
	isReady = true;
	abort = false;

	this->cWidth = 0;
	this->cHeight = 0;
	this->dWidth = 0;
	this->dHeight = 0;
}


/*!
	Stops the execution of the thread and destroys the RecognizeThread object.
*/
RecognizeThread::~RecognizeThread()
{
	stop();
}


/*!
	Stops the execution of the thread.
*/
void RecognizeThread::stop()
{
	mutex.lock();
	abort = true;
	condition.wakeOne();
	mutex.unlock();
	
	wait();
}


/*!
	Adds new object to object recognizer.
*/
void RecognizeThread::trainObject(const QString &objectName, const QString &fileName)
{
	QMutexLocker locker(&mutex);

	if (!recognizer->addObject(colorImage, depthImage, mask))
		return;

	int maxView = -1;
	for (int i = 0; i < objectList.size(); ++i) {
		if (objectList[i].name == objectName && objectList[i].view > maxView)
			maxView = objectList[i].view;
	}

	RecognizableObject object = { objectList.size(), maxView + 1, objectName };
	objectList.push_back(object);

	// Set alpha channel to 255, i.e. make color image fully opaque.
	std::vector<cv::Mat> channels;
	cv::split(this->colorImage, channels);
	channels[3] = cv::Scalar(255);
	cv::merge(channels, this->colorImage);

	std::string fileNamePrefix = QString("%1_%2_").arg(fileName).arg(maxView + 1).toStdString();

	cv::imwrite(fileNamePrefix + "color.png", colorImage);
	cv::imwrite(fileNamePrefix + "depth.png", depthImage);
	cv::imwrite(fileNamePrefix + "mask.png", mask);

}

/*!
	Configures object recognition algorithm using \a configurationFileName and \a cameraSettings.

	\note When the thread is running this function has no effect.
*/
void RecognizeThread::setConfigFile(QString fileName)
{
	if (isRunning())
		return;

	QMutexLocker locker(&mutex);

	isReady = true;
	this->configurationFileName = fileName;
}


/*!
	Runs object recognition algorithm on given \a colorImage and \a alignedDepthImage. During the first call this function automatically starts the execution of the thread.

	\note If the thread is still processing images from previous call this function has no effect.
*/
void RecognizeThread::recognize(const cv::Mat &colorImage, const cv::Mat &depthImage)
{
	QMutexLocker locker(&mutex);

	if (!isReady)
		return;

	isReady = false;

	this->colorImage = colorImage.clone();
	this->depthImage = depthImage.clone();

	if (!isRunning()) {
		abort = false;
		start(TimeCriticalPriority);
	}
	else {
		condition.wakeOne();
	}
}


/*!
	The starting point for the thread. This function is called automatically by the thread after calling recognize() for the first time.
*/
void RecognizeThread::run()
{
	runRecognition();

	mutex.lock();
	// isReady = true;
	objectList.clear();
	recognizer = NULL;
	mutex.unlock();
}


/*!
	Processes camera images and recognize object.
*/
void RecognizeThread::runRecognition()
{        

	cv::Mat depthMask;
	cv::Mat nonAligned_depthMask;
	std::vector<std::vector<cv::Point>> contours;

	forever {
		
		cv::Mat depth16U2;
		
		this->kcv_sensor->alignDepthFrame(depthImage, depthImage.cols, depthImage.rows, colorImage.cols, colorImage.rows,
			depth16U2, depthImage.cols, depthImage.rows);

		emit depthImageReady(depth16U2);
		//removeBlackLines(depth16U2, depth16U2, BLR_MEDIAN_BLUR);

		depthMask.create(depthImage.rows + 2, depthImage.cols + 2, CV_8UC1);                                // Set depth mask pixels to zero values.
		depthMask = BLACK;
		// floodfill
		int minDepth = floodFillSegmentation(depth16U2, depthMask);                                          // Compute object mask in aligned depth map coordinate system.
		
		cv::findContours(depthMask, contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);
		cv::drawContours(depthMask, contours, 0, WHITE, CV_FILLED);                                         // Fill holes in depth mask.
		
		//
		cv::resize(this->colorImage, this->colorImage, cv::Size(this->cWidth, this->cHeight));
		cv::resize(this->depthImage, this->depthImage, cv::Size(this->dWidth, this->dHeight));
		//

		if (colorImage.size() == depthImage.size())														    // Resize object mask to color image size.
			mask = depthMask(cv::Rect(1, 1, depthImage.cols, depthImage.rows)).clone();
		else
			cv::resize(depthMask(cv::Rect(1, 1, depthImage.cols, depthImage.rows)), mask, cv::Size(colorImage.cols, colorImage.rows), 0.0, 0.0, cv::INTER_NEAREST);
		
		
		int recognizedObjectIndex = recognizer->recognizeObject(colorImage, depthImage, mask);
		
		if (recognizedObjectIndex < 0)
		{
			emit objectRecognized(QString());
		}
		else
		{
			emit objectRecognized(objectList[recognizedObjectIndex].name);
		}

		// Apply mask on color image.
		maskedColorImage.create(colorImage.rows, colorImage.cols, CV_8UC4);
		maskedColorImage = BLACK;
		colorImage.copyTo(maskedColorImage, mask);
		cv::resize(maskedColorImage, maskedColorImage, cv::Size(depthImage.cols,depthImage.rows));

		emit segmentedImageReady(maskedColorImage);

        mutex.lock();
		isReady = true;
		if (abort) {
			mutex.unlock();
			return;
		}
        condition.wait(&mutex);
        mutex.unlock();
    }
}


/*!
	Initializes object recognizer object and object list.
*/
bool RecognizeThread::loadSettings()
{
	mutex.lock();
	// Check if configuration file exists.
	QFileInfo fileInfo(configurationFileName);
	if (!fileInfo.exists()) {
		emit configurationFileNotFound(configurationFileName);
		mutex.unlock();
		return false;
	}

	// Open configuration file.
	QSettings settings(configurationFileName, QSettings::IniFormat);
	settings.setIniCodec("UTF-8");

	// Check if an access or format error occurred.
	QSettings::Status status = settings.status();
	if (status != QSettings::NoError) {
		emit configurationFileErrorStatus(configurationFileName, (int) status);
		mutex.unlock();
		return false;
	}

	cv::initModule_nonfree();

	// create object for depth sensor
	kcv_sensor = kcv::KCV_sensor::getInstance();
	settings.beginGroup("SensorConfig");
	settings.beginGroup("sparameters");
	QStringList parameterList2 = settings.childKeys();
	int si = parameterList2.size();
	this->cWidth = settings.value("colorWidth",0).toInt();
	this->cHeight = settings.value("colorHeight", 0).toInt();
	this->dWidth = settings.value("depthWidth", 0).toInt();
	this->dHeight = settings.value("depthHeight", 0).toInt();
	
	HRESULT hr = kcv_sensor->initSensor(cWidth, cHeight, dWidth, dHeight);
	settings.endGroup();
	settings.endGroup();

	settings.beginGroup("DescriptorConfig");
	
	recognizer = ObjectRecognizer::create(settings.value("descriptorName", "").toString().toStdString());
	if (recognizer.empty()) {
		emit unsupportedRecognizerType(settings.value("type", "").toString());
		mutex.unlock();
		return false;
	}
	settings.endGroup();

	// matching parameters
	QString parameter;
	settings.beginGroup("MatchingConfig");
	settings.beginGroup("mparameters");
	QStringList parameterList = settings.childKeys();
	foreach(parameter, parameterList)
		setParameter(recognizer, parameter.toStdString(), settings.value(parameter));
	settings.endGroup();
	settings.endGroup();
	
	recognizer->initialize(cWidth, cHeight);

	// Create object list.
	QString path(fileInfo.path());
	QString fileName;
	RecognizableObject object;
	int objectId = 0;
	kcv_sensor->isTraining = true;

	int numObjects = settings.beginReadArray("objects");
	for (int i = 0; i < numObjects; ++i) {
		settings.setArrayIndex(i);
		object.name = settings.value("name").toString();
		int numViews = settings.beginReadArray("views");
		for (int j = 0; j < numViews; ++j) {
			settings.setArrayIndex(j);
			object.id = objectId;
			object.view = j;
			objectList.push_back(object);

			fileName = path + settings.value("color", "").toString();
			colorImage = cv::imread(fileName.toStdString(), CV_LOAD_IMAGE_COLOR);
			if (colorImage.empty()) {
				emit imageNotFound(fileName);
				mutex.unlock();
				return false;
			}

			fileName = path + settings.value("depth", "").toString();
			depthImage = cv::imread(fileName.toStdString(), CV_LOAD_IMAGE_ANYDEPTH);
			if (depthImage.empty()) {
				emit imageNotFound(fileName);
				mutex.unlock();
				return false;
			}

			fileName = path + settings.value("mask", "").toString();
			mask = cv::imread(fileName.toStdString(), CV_LOAD_IMAGE_GRAYSCALE);
			if (mask.empty()) {
				emit imageNotFound(fileName);
				mutex.unlock();
				return false;
			}

			recognizer->addObject(colorImage, depthImage, mask);
			++objectId;
		}
		settings.endArray();
	}
	settings.endArray();

	kcv_sensor->isTraining = false;

	mutex.unlock();

	if (SUCCEEDED(hr))
		return true;
	return false;
}


/*!
	Sets a \a parameter of an \a algorithm with a specified \value.
*/
void RecognizeThread::setParameter(cv::Algorithm *algorithm, const std::string &parameter, const QVariant &value)
{
	switch (algorithm->paramType(parameter)) {
        case cv::Param::BOOLEAN: algorithm->set(parameter, value.toBool());   break;
		case cv::Param::INT:     algorithm->set(parameter, value.toInt());    break;
		case cv::Param::FLOAT:   algorithm->set(parameter, value.toFloat());  break;
		case cv::Param::REAL:    algorithm->set(parameter, value.toDouble()); break;
		case cv::Param::STRING:  algorithm->set(parameter, value.toString().toStdString()); break;
		default: /* cv::Param::MAT, cv::Param::ALGORITHM, cv::Param::MAT_VECTOR */ break;
    }
}


/*!
	Displays an \a image in a window with specified \a title. If \a binary is true the image is displayed in black & white colors, otherwise it is displayed in colors specified by the cv::Mat::type().
*/
void RecognizeThread::display(const cv::Mat &image, QString &title, bool binary)
{
	if (binary) {
		cv::Mat black(cv::Size(image.cols, image.rows), CV_8UC1, BLACK);
		images[title] = black.setTo(WHITE, image);
	}
	else {
		images[title] = image.clone();
	}

	emit imageReady(images[title], title);
}


/*!
	Removes various artefacts from \a src using specified \a method. The result is stored in \a dst.
*/
void RecognizeThread::removeBlackLines(const cv::Mat &src, cv::Mat &dst, BLACK_LINES_REMOVAL_METHOD method)
{
	switch (method) {
	case BLR_MEDIAN_BLUR:
		cv::medianBlur(src, dst, 3);
		break;
	case BLR_DILATATION:
		cv::dilate(src, dst, cv::getStructuringElement(cv::MORPH_RECT, cv::Size(3, 3)));
		break;
	case BLR_CLOSING:
		cv::morphologyEx(src, dst, cv::MORPH_CLOSE, cv::getStructuringElement(cv::MORPH_RECT, cv::Size(3, 3)));
		break;
	default:
		dst = src.clone();
		break;
	}
}


/*!
	Computes object segmentation \a mask from \a depthImage using growing regions algorithm.
*/
int RecognizeThread::floodFillSegmentation(const cv::Mat &depthImage, cv::Mat &mask)
{
	cv::Mat depth32S;
	depthImage.convertTo(depth32S, CV_32SC1);	

	cv::Point seed;
	int minDepth = findSeedPoint(depthImage, seed);

	cv::floodFill(depth32S, mask, seed, BLACK, NULL, cv::Scalar(8), cv::Scalar(8), cv::FLOODFILL_MASK_ONLY); //20

	return minDepth;
}


/*!
	Finds \a seed point (closes valid point) for flood fill operation in \a depthImage.
*/
int RecognizeThread::findSeedPoint(const cv::Mat &depthImage, cv::Point &seed)
{
	// Find seed position (closest point in depth map).
	ushort minimum = USHRT_MAX;
	int position = -1;

	int numPixels = depthImage.cols * depthImage.rows;
	const ushort *p = (ushort *) depthImage.data;
	for (int i = 0; i < numPixels; ++i) {
		if (p[i] != 0 && minimum > p[i]) {
			minimum = p[i];
			position = i;
		}
    }
	
	// Create seed point.
	if (position >= 0) {
		seed.x = position % depthImage.cols;
		seed.y = position / depthImage.cols;
	}
	else {
		seed.x = depthImage.cols / 2;
		seed.y = depthImage.rows / 2;
	}

	return minimum;
}
