//    File: imagewidget.cpp
//
//	  Date: February 9, 2013
//
//  Author: Marek Racev


#include "imagewidget.h"
#include <QtGui>


const qreal zoomInFactor = 1.5;
const qreal zoomOutFactor = 1.0 / zoomInFactor;
const qreal maxScaleFactor = 100.0;
const qreal minScaleFactor = 0.01;


/*!
	\class ImageWidget
	\brief Zoomable image widget.
*/


/*!
	Constructs a ImageWidget object.
*/
ImageWidget::ImageWidget(QWidget *parent)
	: QGraphicsView(parent)
{
}


/*!
	Destroys a ImageWidget object.
*/
ImageWidget::~ImageWidget()
{
}


/*!
	Processes pressed key events.
*/
void ImageWidget::keyPressEvent(QKeyEvent *event)
{
	switch (event->key()) {
	case Qt::Key_Plus:
		zoom(zoomInFactor);
		break;
	case Qt::Key_Minus:
		zoom(zoomOutFactor);
		break;
	default:
		QGraphicsView::keyPressEvent(event);
		break;
    }
}


/*!
	Processes double mouse click events.
*/
void ImageWidget::mouseDoubleClickEvent(QMouseEvent *event)
{
	fitInView(sceneRect(), Qt::KeepAspectRatio);
	//setTransform(QTransform());
}


/*!
	Processes mouse wheel events.
*/
void ImageWidget::wheelEvent(QWheelEvent *event)
{
    zoom(pow(2.0, -event->delta() / 240.0));
}


/*!
	Zooms in and out depending on \a scaleFactor value.
*/
void ImageWidget::zoom(qreal scaleFactor)
{
	qreal factor = transform().scale(scaleFactor, scaleFactor).mapRect(QRectF(0.0, 0.0, 1.0, 1.0)).width();
	if (factor < minScaleFactor || factor > maxScaleFactor)
		return;
	
	scale(scaleFactor, scaleFactor);
}
