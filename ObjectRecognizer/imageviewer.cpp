//    File: imageviewer.cpp
//
//	  Date: February 11, 2013
//
//  Author: Marek Racev


#include "imageviewer.h"
#include "ui_imageviewer.h"
#include "imagewidget.h"
#include <QPixmap>


/*!
	\class ImageViewer
	\brief Image Viewer dialog.
*/


/*!
	Constructs a ImageViewer object.
*/
ImageViewer::ImageViewer(QWidget *parent) :
	QDialog(parent),
	ui(new Ui::ImageViewer)
{
	ui->setupUi(this);
	text.clear();

	scene = new QGraphicsScene(this);
	ui->graphicsView->setScene(scene);

	statusBar = new QStatusBar(this);
	ui->dialogLayout->addWidget(statusBar);
	resize(640, 480 + statusBar->height() - 10);
	
	time.start();
	frames = 0;
	initialTime.start();
	totalFrames = 0;
}


/*!
	Destroys a ImageViewer object.
*/
ImageViewer::~ImageViewer()
{
	delete ui;
}


/*!
	Shows an \a image.
*/
void ImageViewer::showImage(QImage &image)
{
	//if (!mutex.tryLock())
	//	return;
	showText(image);
	scene->clear();
	scene->addPixmap(QPixmap().fromImage(image));
	scene->setSceneRect(0.0, 0.0, image.width(), image.height());
	showFps();
	//mutex.unlock();
}


/*!
	Sets \a scaleFactor of graphics view widget.
*/
void ImageViewer::setScaleFactor(double scaleFactor)
{
	ui->graphicsView->zoom(scaleFactor);
}


/*!
	Draws text on \a image.
*/
void ImageViewer::showText(QImage &image)
{
	if (text.isEmpty())
		return;

	QPainter painter(&image);
	painter.setPen(Qt::white);
	painter.setFont(QFont("Arial", 40 * image.width() / 640));
	painter.drawText(0, 10 * image.width() / 640, image.width(), image.height(), Qt::AlignTop | Qt::AlignHCenter | Qt::TextWordWrap, text);
}


/*!
	Shows current and average FPS.
*/
void ImageViewer::showFps()
{
	++frames;
	if (time.elapsed() < 1000)
		return;
	totalFrames += frames;
	statusBar->showMessage(tr("Current FPS: %1    Average FPS: %2").arg((frames * 1000.0) / time.restart()).arg((totalFrames * 1000.0) / initialTime.elapsed()));
	frames = 0;
}


/*!
	Sets \a text that should be drawn on image.
*/
void ImageViewer::setText(const QString &text)
{
	this->text = text;
}

void ImageViewer::delay(int miliseconds)
{
	QTime dieTime = QTime::currentTime().addMSecs(miliseconds);
	while (QTime::currentTime() < dieTime)
		QCoreApplication::processEvents(QEventLoop::AllEvents, miliseconds);
}