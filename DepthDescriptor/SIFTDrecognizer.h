//    File: SIFTDrecognizer.h
//
//	  Date: April, 2015
//
//  Author: Marek Jakab, Marek Racev

#include "objectrecognizer.h"

#include <opencv2/core/core.hpp>
#include <opencv2/features2d/features2d.hpp>
#include <opencv2/nonfree/features2d.hpp>
#include <opencv2/nonfree/nonfree.hpp>

#include <SiftGPU.h>

#include "depthdescriptor.h"

class SIFTDrecognizer : public ObjectRecognizer
{
public:
	SIFTDrecognizer();
	SIFTDrecognizer(bool withDepth);
	//SIFTDrecognizer(cv::Ptr<cv::FeatureDetector> featureDetector, cv::Ptr<cv::DescriptorExtractor> descriptorExtractor, cv::Ptr<cv::DescriptorMatcher> descriptorMatcher, cv::Ptr<kcv::KCV_sensor> kcv_sensor, cv::Ptr<SiftGPU> siftGpu);
	SIFTDrecognizer(cv::Ptr<cv::FeatureDetector> featureDetector, cv::Ptr<cv::DescriptorExtractor> descriptorExtractor, cv::Ptr<cv::DescriptorMatcher> descriptorMatcher, kcv::KCV_sensor *kcv_sensor, cv::Ptr<SiftGPU> siftGpu);
	~SIFTDrecognizer();

	bool isInitialized() const;

	cv::AlgorithmInfo *info() const;

#ifdef _DEBUG
	const std::vector<cv::Mat> &getIntensityImages() { return intensityImages; }
	const std::vector<cv::Mat> &getDepthImages() { return depthImages; }
	const std::vector<cv::Mat> &getMasks() { return masks; }
	const std::vector<cv::Mat> &getMaskedIntensityImages() { return maskedIntensityImages; }
	const cv::Mat &getIntensityImage() { return intensityImage; }
	const cv::Mat &getDepthImage() { return depthImage; }
	const cv::Mat &getMask() { return mask; }
	const cv::Mat &getMaskedIntensityImage() { return maskedIntensityImage; }
	const std::vector<std::vector<cv::KeyPoint>> &getKeypoints() { return keypoints; }

	const std::vector<cv::KeyPoint> &getObjectKeypoints() { return objectKeypoints; }
	const std::vector<cv::Mat> &getDescriptors() { return descriptors; }
	const cv::Mat &getObjectDescriptors() { return objectDescriptors; }
	const std::vector<std::vector<cv::DMatch>> &getMatches() { return matches; }

	const std::vector<int> &getIndicesPastDepth() { return indicesPassedDepth; }
#endif

private:
	void initializeParameters(int cWidth, int cHeight);
	bool addObjectImpl(const cv::Mat &colorImage, const cv::Mat &depthImage, const cv::Mat &mask);
	int recognizeObjectImpl(const cv::Mat &colorImage, const cv::Mat &depthImage, const cv::Mat &mask);
	int getNumberOfObj();

private:
	void processImages(const cv::Mat &colorImage, const cv::Mat &depthImage, const cv::Mat &mask);

private:

	bool depth = false;

	DepthD::DepthDescriptor *depthDescriptor = NULL;

	std::vector<cv::Mat> intensityImages;
	std::vector<cv::Mat> depthImages;
	std::vector<cv::Mat> masks;
	std::vector<cv::Mat> maskedIntensityImages;

	cv::Mat intensityImage;
	cv::Mat depthImage;
	cv::Mat mask;
	cv::Mat maskedIntensityImage;

	std::vector<std::vector<cv::KeyPoint>> keypoints;
	std::vector<cv::KeyPoint> objectKeypoints;

	std::vector<cv::Mat> descriptors;
	cv::Mat objectDescriptors;

	std::vector<std::vector<cv::DMatch>> matches;
	std::vector<int> indicesPassedDepth;

	std::vector<std::vector<cv::Point2f>> points;
	std::vector<std::vector<cv::Point2f>> objectPoints;

	cv::Ptr<cv::FeatureDetector> featureDetector;
	cv::Ptr<cv::DescriptorExtractor> descriptorExtractor;
	cv::Ptr<cv::DescriptorMatcher> descriptorMatcher;
	cv::Ptr<SiftGPU> siftGpu;
	kcv::KCV_sensor *kcv_sensor;

	int minKeypointsPerObject;
	double distanceRatioThreshold;
	double minRecognitionConfidence;
	int minMatchesToFindHomography;
	bool ransacOutliersRemovalEnabled;
	float patternSize;
};