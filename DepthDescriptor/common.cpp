//    File: common.cpp
//
//	  Date: April, 2015
//
//  Author: Marek Jakab, Marek Racev


#include "common.h"

#include <windows.h>
#include <gl/GL.h>
const int SIFT_DIMENSIONS = 128;

#include <opencv2/nonfree/features2d.hpp>
#include <opencv2/nonfree/nonfree.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <opencv2/highgui/highgui.hpp>

#include <iostream>
#include <fstream>

//#define KP_TEST

/*!
Runs FAST kp detection on an \a depthImage with \a mask and detects \a keypoints with set \a threshold.
*/
void runFastDepthKP(const cv::Mat &depthImage, const cv::Mat &mask, std::vector<cv::KeyPoint> &keypoints, int threshold)
{
	cv::Mat gray, maskedDepth;
	depthImage.copyTo(maskedDepth, mask);
	cv::cvtColor(maskedDepth, gray, CV_BGR2GRAY);
	cv::FAST(gray, keypoints, threshold, true);
}

/*!
Runs FAST kp detection on an gpu \a depthImage with \a mask and detects \a keypoints with set \a threshold.
*/
void runFastDepthKP(const cv::gpu::GpuMat &depthImage, const cv::gpu::GpuMat &mask, std::vector<cv::KeyPoint> &keypoints, int threshold)
{
	if (cv::gpu::getCudaEnabledDeviceCount() < 1)
		return;
	cv::gpu::GpuMat gray(depthImage.size(),CV_8UC1);
	cv::gpu::cvtColor(depthImage, gray, CV_BGR2GRAY);
	cv::gpu::FAST_GPU fastGPU = cv::gpu::FAST_GPU(threshold, true);

	fastGPU.calcKeyPointsLocation(gray, mask);
	cv::gpu::GpuMat kp;
	fastGPU.getKeyPoints(kp);
	fastGPU.downloadKeypoints(kp, keypoints);
}


/*!
Runs \a orbGpu on an intensity \a image and detects \a keypoints and extracts \a descriptors in input \a mask.
*/
void runOrbGpu(cv::gpu::ORB_GPU &orbGpu, const cv::gpu::GpuMat &image, const cv::gpu::GpuMat &mask, cv::gpu::GpuMat &keypoints, cv::gpu::GpuMat &descriptors)
{
	orbGpu.blurForDescriptor = true;
	cv::gpu::GpuMat computed_keypoints,computed_descriptors;
	orbGpu(image, mask, computed_keypoints, computed_descriptors);

	if (computed_keypoints.cols == 0 || computed_descriptors.rows == 0)
		return;

#ifdef KP_TEST
	LARGE_INTEGER frequency;        // ticks per second
	LARGE_INTEGER t1, t2, t3, t4, t5;           // ticks
	//double elapsedTime;
	QueryPerformanceFrequency(&frequency);
	std::ofstream myfile;
	myfile.open("kp_border_erase.txt", std::ios::out | std::ios::app);
	QueryPerformanceCounter(&t1);
#endif
	cv::Mat cpu_keypoints;
	cv::Mat cpu_descriptors;
	computed_keypoints.download(cpu_keypoints);
	computed_descriptors.download(cpu_descriptors);

	cv::Mat cpuMask;
	cv::Mat cpuErodedMask;
	mask.download(cpuMask);
	const int elementSize = image.cols / 64 + 1;
	cv::erode(cpuMask, cpuErodedMask, cv::getStructuringElement(cv::MORPH_RECT, cv::Size(elementSize, elementSize)));

	cv::Mat dst_cpu_keypoints = cv::Mat(cpu_keypoints.rows, 0, cpu_keypoints.type()); // cols = 0
	cv::Mat dst_cpu_descriptors = cv::Mat(0, cpu_descriptors.cols, cpu_descriptors.type()); // rows = 0

	for (int i = 0; i < cpu_keypoints.cols; ++i) {

		if (cpuErodedMask.at<uchar>((int)(cpu_keypoints.at<float>(1, i)), (int)(cpu_keypoints.at<float>(0, i))) != 0)
		{
			if (dst_cpu_keypoints.cols == 0)
				cv::vconcat(cpu_keypoints.col(i), dst_cpu_keypoints);
			else
				cv::hconcat(dst_cpu_keypoints, cpu_keypoints.col(i), dst_cpu_keypoints);
			dst_cpu_descriptors.push_back(cpu_descriptors.row(i));
		}
	}
	if (dst_cpu_descriptors.rows <= 0)
		return;
	keypoints.upload(dst_cpu_keypoints);
	descriptors.upload(dst_cpu_descriptors);
#ifdef KP_TEST
	QueryPerformanceCounter(&t2);
	myfile << ((t2.QuadPart - t1.QuadPart) * 1000.0 / frequency.QuadPart) << " overall \n";
	myfile.close();
#endif
}

/*!
Runs \a siftGpu on an intensity \a image and detects \a keypoints and extracts \a descriptors in input \a mask.
*/
void runSiftGpu(cv::Ptr<SiftGPU> &siftGpu, const cv::Mat &image, std::vector<cv::KeyPoint> &keypoints, cv::Mat &descriptors, const cv::Mat &mask)
{
	if (!siftGpu->RunSIFT(image.cols, image.rows, image.data, GL_LUMINANCE, GL_UNSIGNED_BYTE)) {
		keypoints.resize(0);
		descriptors = cv::Mat();
		return;
	}

	const int numFeatures = siftGpu->GetFeatureNum();
	SiftGPU::SiftKeypoint *siftKeypoints = new SiftGPU::SiftKeypoint[numFeatures];
	descriptors.create(numFeatures, SIFT_DIMENSIONS, CV_32FC1);
	siftGpu->GetFeatureVector(siftKeypoints, (float *)descriptors.data);


#ifdef KP_TEST
	LARGE_INTEGER frequency;        // ticks per second
	LARGE_INTEGER t1, t2;           // ticks
	//double elapsedTime;
	QueryPerformanceFrequency(&frequency);
	std::ofstream myfile;
	myfile.open("kp_border_erase.txt", std::ios::out | std::ios::app);
	QueryPerformanceCounter(&t1);
#endif
	cv::Mat erodedMask;
	const int elementSize = image.cols / 64 + 1;
	cv::erode(mask, erodedMask, cv::getStructuringElement(cv::MORPH_RECT, cv::Size(elementSize, elementSize)));

	keypoints.resize(0);
	keypoints.reserve(numFeatures);

	int lastBadDescriptorIndex = -1;
	cv::KeyPoint keypoint;
	cv::Point2f *point;

	for (int i = 0; i < numFeatures; ++i) {
		point = (cv::Point2f *) &siftKeypoints[i];
		if (erodedMask.at<uchar>(*point) != 0) {
			keypoint.pt = *point;
			//keypoint.size = siftKeypoints[i].s;
			//keypoint.angle = siftKeypoints[i].o;
			keypoints.push_back(keypoint);
			if (lastBadDescriptorIndex < 0)
				continue;

			std::memcpy(descriptors.ptr<float>(lastBadDescriptorIndex), descriptors.ptr<float>(i), SIFT_DIMENSIONS * sizeof(float));
			++lastBadDescriptorIndex;
		}
		else if (lastBadDescriptorIndex < 0)
			lastBadDescriptorIndex = i;
	}

	delete[] siftKeypoints;

	if (lastBadDescriptorIndex < 0)
		return;
	descriptors.resize(lastBadDescriptorIndex);
#ifdef KP_TEST
	QueryPerformanceCounter(&t2);
	myfile << ((t2.QuadPart - t1.QuadPart) * 1000.0 / frequency.QuadPart) << "\n";
#endif
}


/*!
Converts \a numKeypoints of \a siftKeypoints to \a cvKeypoints.

\note This function is intended for debugging purposes.
*/
void convertKeypoints(const SiftGPU::SiftKeypoint siftKeypoints[], int numKeypoints, std::vector<cv::KeyPoint> &cvKeypoints)
{
	cvKeypoints.resize(0);
	cvKeypoints.reserve(numKeypoints);

	cv::KeyPoint keypoint;
	for (int i = 0; i < numKeypoints; ++i) {
		keypoint.pt.x = siftKeypoints[i].x;
		keypoint.pt.y = siftKeypoints[i].y;
		cvKeypoints.push_back(keypoint);
	}
}


/*!
Runs \a featureDetector on an intensity \a image and detects \a keypoints in input \a mask.
*/
void siftDetect(cv::Ptr<cv::FeatureDetector> &featureDetector, const cv::Mat &image, std::vector<cv::KeyPoint>& keypoints, const cv::Mat &mask)
{
	featureDetector->detect(image, keypoints);

	cv::Mat erodedMask;
	const int elementSize = image.cols / 64 + 1;
	cv::erode(mask, erodedMask, cv::getStructuringElement(cv::MORPH_RECT, cv::Size(elementSize, elementSize)));

	int lastBadKeypointIndex = -1;
	for (int i = 0; i < keypoints.size(); ++i) {
		if (erodedMask.at<unsigned char>(keypoints[i].pt) != 0) {
			if (lastBadKeypointIndex < 0)
				continue;
			keypoints[lastBadKeypointIndex] = keypoints[i];
			++lastBadKeypointIndex;
		}
		else if (lastBadKeypointIndex < 0)
			lastBadKeypointIndex = i;
	}

	if (lastBadKeypointIndex < 0)
		return;
	keypoints.resize(lastBadKeypointIndex);
}


/*!
Computes \a matches from \a queryDescriptors and vector of \a trainDescriptors with \a descriptorMatcher using robust matching strategy with distance \a ratioThreshold.
*/
void robustMatching(cv::Ptr<cv::DescriptorMatcher> &descriptorMatcher, const cv::Mat &queryDescriptors, const std::vector<cv::Mat> &trainDescriptors, std::vector<std::vector<cv::DMatch>> &matches, double ratioThreshold)
{
	for (int i = 0; i < matches.size(); ++i)
		robustMatching(descriptorMatcher, queryDescriptors, trainDescriptors[i], matches[i], ratioThreshold);
}


/*!
Computes \a matches from \a queryDescriptors and \a trainDescriptors with \a descriptorMatcher using robust matching strategy with distance \a ratioThreshold.
*/
void robustMatching(cv::Ptr<cv::DescriptorMatcher> &descriptorMatcher, const cv::Mat &queryDescriptors, const cv::Mat &trainDescriptors, std::vector<cv::DMatch> &matches, double ratioThreshold)
{
	std::vector<std::vector<cv::DMatch>> knnMatches1;
	descriptorMatcher->knnMatch(queryDescriptors, trainDescriptors, knnMatches1, 2);
	distanceRatioFilter(knnMatches1, ratioThreshold);

	std::vector<std::vector<cv::DMatch>> knnMatches2;
	descriptorMatcher->knnMatch(trainDescriptors, queryDescriptors, knnMatches2, 2);
	distanceRatioFilter(knnMatches2, ratioThreshold);

	crossCheckFilter(knnMatches1, knnMatches2, matches);
}


/*!
Filters out \a knnMatches with distance ratio greater than \a ratioThreshold.
*/
void distanceRatioFilter(std::vector<std::vector<cv::DMatch>> &knnMatches, double ratioThreshold)
{
	for (int i = 0; i < knnMatches.size(); ++i) {
		//if (knnMatches[i].size() < 2)
		//	continue;

		if (knnMatches[i][0].distance / knnMatches[i][1].distance > ratioThreshold)
			knnMatches[i].clear();
	}
}


/*!
Extracts symmertrical \a matches from \a knnMatches1 and \a knnMatches2.
*/
void crossCheckFilter(const std::vector<std::vector<cv::DMatch>> &knnMatches1, const std::vector<std::vector<cv::DMatch>> &knnMatches2, std::vector<cv::DMatch> &matches)
{
	matches.resize(0);
	matches.reserve(maximum(knnMatches1.size(), knnMatches2.size()));

	cv::DMatch forward;
	for (int i = 0; i < knnMatches1.size(); ++i) {
		if (knnMatches1[i].size() < 2)
			continue;

		forward = knnMatches1[i][0];
		if (knnMatches2[forward.trainIdx].size() < 2)
			continue;

		if (knnMatches2[forward.trainIdx][0].trainIdx == forward.queryIdx)
			matches.push_back(forward);
	}
}


/*!
Runs \a descriptorMatcher on \a queryDescriptors and vector of \a trainDescriptors and returns unidirectional \a matches that pass distance \a ratioThreshold test.
*/
void unidirectionalMatching(cv::Ptr<cv::DescriptorMatcher> &descriptorMatcher, const cv::Mat &queryDescriptors, const std::vector<cv::Mat> &trainDescriptors, std::vector<std::vector<cv::DMatch>> &matches, double ratioThreshold)
{
	for (int i = 0; i < matches.size(); ++i)
		unidirectionalMatching(descriptorMatcher, queryDescriptors, trainDescriptors[i], matches[i], ratioThreshold);
}


/*!
Runs \a descriptorMatcher on \a queryDescriptors and \a trainDescriptors and returns unidirectional \a matches that pass distance \a ratioThreshold test.
*/
void unidirectionalMatching(cv::Ptr<cv::DescriptorMatcher> &descriptorMatcher, const cv::Mat &queryDescriptors, const cv::Mat &trainDescriptors, std::vector<cv::DMatch> &matches, double ratioThreshold)
{
	std::vector<std::vector<cv::DMatch>> knnMatches;
	descriptorMatcher->knnMatch(queryDescriptors, trainDescriptors, knnMatches, 2);
	distanceRatioFilter(knnMatches, matches, ratioThreshold);
}


/*!
Copies \a knnMatches with distance ratio lower than \a ratioThreshold to \a matches.
*/
void distanceRatioFilter(const std::vector<std::vector<cv::DMatch>> &knnMatches, std::vector<cv::DMatch> &matches, double ratioThreshold)
{
	matches.resize(0);
	matches.reserve(knnMatches.size());

	for (int i = 0; i < knnMatches.size(); ++i) {
		//if (knnMatches[i].size() < 2)
		//	continue;

		if (knnMatches[i][0].distance / knnMatches[i][1].distance < ratioThreshold)
			matches.push_back(knnMatches[i][0]);
	}
}


/*!
Computes \a matches from \a queryDescriptors and vector of \a trainDescriptors with \a descriptorMatcher using simple matching strategy.
*/
void simpleMatching(cv::Ptr<cv::DescriptorMatcher> &descriptorMatcher, const cv::Mat &queryDescriptors, const std::vector<cv::Mat> &trainDescriptors, std::vector<std::vector<cv::DMatch>> &matches)
{
	for (int i = 0; i < matches.size(); ++i)
		descriptorMatcher->match(queryDescriptors, trainDescriptors[i], matches[i]);
}


/*!
Filters good matches from a vector of \a matches. If distance between two keypoint descriptors is lower than \a maxDescriptorDistanceFactor * minDistance the match defined by these keypoints is preserved in the vector.
In case that \a maxDescriptorDistanceFactor <= 1.0 all found matches are preserved. If minDistance > \a minDescriptorDistanceThreshold vector of \a matches is resized to 0. In case that \a minDescriptorDistanceThreshold <= 0.0 this parameter has no efect.
*/
void filterGoodMatches(std::vector<cv::DMatch> &matches, double maxDescriptorDistanceFactor, double minDescriptorDistanceThreshold)
{
	if (maxDescriptorDistanceFactor <= 1.0)
		return;

	double distance;
	double minDistance = 1000.0;
	//double maxDistance = 0.0;

	// Compute minimal distance between keypoints.
	for (int i = 0; i < matches.size(); ++i) {
		distance = matches[i].distance;
		if (distance < minDistance)
			minDistance = distance;
		//if (distance > maxDistance)
		//	maxDistance = distance;
	}

	if (minDescriptorDistanceThreshold > 0.0) {
		if (minDistance > minDescriptorDistanceThreshold) {
			matches.resize(0);
			return;
		}
	}

	// Filter good matches.
	int lastBadMatchIndex = -1;
	for (int i = 0; i < matches.size(); ++i) {
		if (matches[i].distance < maxDescriptorDistanceFactor * minDistance) {
			if (lastBadMatchIndex < 0)
				continue;
			matches[lastBadMatchIndex] = matches[i];
			++lastBadMatchIndex;
		}
		else if (lastBadMatchIndex < 0)
			lastBadMatchIndex = i;
	}

	if (lastBadMatchIndex < 0)
		return;
	matches.resize(lastBadMatchIndex);
}


/*!
Filters good \a matches with distance below global \a minDescriptorDistanceThreshold.
*/
int filterGoodMatches(std::vector<cv::DMatch> &matches, double minDescriptorDistanceThreshold)
{
#ifdef _DEBUG
	int lastBadMatchIndex = -1;

	for (int i = 0; i < matches.size(); ++i) {
		if (matches[i].distance < minDescriptorDistanceThreshold) {
			if (lastBadMatchIndex < 0)
				continue;
			matches[lastBadMatchIndex] = matches[i];
			++lastBadMatchIndex;
		}
		else if (lastBadMatchIndex < 0)
			lastBadMatchIndex = i;
	}

	if (lastBadMatchIndex < 0)
		return matches.size();
	matches.resize(lastBadMatchIndex);

	return matches.size();
#else
	int count = 0;
	for (int i = 0; i < matches.size(); ++i) {
		if (matches[i].distance < minDescriptorDistanceThreshold)
			++count;
	}
	return count;
#endif
}


/*!
Extract inlier \a matches using \a inliers mask.
*/
void extractInlierMatches(std::vector<cv::DMatch> &matches, const std::vector<uchar> &inliers)
{
	int lastBadMatchIndex = -1;
	for (int i = 0; i < matches.size(); ++i) {
		if (inliers[i]) {
			if (lastBadMatchIndex < 0)
				continue;
			matches[lastBadMatchIndex] = matches[i];
			++lastBadMatchIndex;
		}
		else if (lastBadMatchIndex < 0)
			lastBadMatchIndex = i;
	}

	if (lastBadMatchIndex < 0)
		return;
	matches.resize(lastBadMatchIndex);
}


/*!
Computes \a localHistogramsPerDimension^2 \a localHistograms of \a localHistogramsSize from an \a image.
*/
void computeLocalHistograms(const cv::Mat &image, std::vector<cv::Mat> &localHistograms, int localHistogramsPerDimension, int localHistogramsSize)
{
	cv::Mat sector;
	const cv::Mat emptyMask;

	const int sectorWidth = image.cols / localHistogramsPerDimension;
	const int sectorHeight = image.rows / localHistogramsPerDimension;

	localHistograms.resize(localHistogramsPerDimension * localHistogramsPerDimension);

	float range[] = { 0.0, 256.0 };
	const float *histRange = { range };
	int i = 0;

	for (int y = 0; y < image.rows; y += sectorHeight) {
		for (int x = 0; x < image.cols; x += sectorWidth) {
			sector = image(cv::Rect(x, y, sectorWidth, sectorHeight));
			cv::calcHist(&sector, 1, 0, emptyMask, localHistograms[i], 1, &localHistogramsSize, &histRange, true, false);
			cv::normalize(localHistograms[i], localHistograms[i], 0.0, 1.0, cv::NORM_MINMAX, -1);

			++i;
		}
	}
}


/*!
Computes similarity of \a localHistogramsPerDimension^2 \a localHistograms of \a localHistogramsSize and local histograms extracted from an \a image. If similarity exceeds \a objectSimilarityThreshold, the computation is terminated.
*/
double computeObjectSimilarity(const cv::Mat &image, const std::vector<cv::Mat> &localHistograms, double objectSimilarityThreshold, int localHistogramsPerDimension, int localHistogramsSize)
{
	cv::Mat sector;
	const cv::Mat emptyMask;
	cv::Mat objectLocalHistogram;

	const int sectorWidth = image.cols / localHistogramsPerDimension;
	const int sectorHeight = image.rows / localHistogramsPerDimension;

	float range[] = { 0.0, 256.0 };
	const float *histRange = { range };

	int i = 0;
	double similarity = 0.0;
	float blackPixels = 10.0f;

	for (int y = 0; y < image.rows; y += sectorHeight) {
		for (int x = 0; x < image.cols; x += sectorWidth) {
			sector = image(cv::Rect(x, y, sectorWidth, sectorHeight));
			cv::calcHist(&sector, 1, 0, emptyMask, objectLocalHistogram, 1, &localHistogramsSize, &histRange, true, false);
			blackPixels += objectLocalHistogram.at<float>(0, 0);
			cv::normalize(objectLocalHistogram, objectLocalHistogram, 0.0, 1.0, cv::NORM_MINMAX, -1);
			similarity += cv::compareHist(objectLocalHistogram, localHistograms[i], CV_COMP_BHATTACHARYYA);
			if (similarity > objectSimilarityThreshold)
				return similarity;
			++i;
		}
	}

	if (blackPixels > image.cols * image.rows)
		return blackPixels;

	return similarity;
}


/*!
Visualizes vector of \a localHistogramsPerDimension^2 \a histograms into single image using \a lineWidth and \a histogramHeight.
*/
cv::Mat visualizeHistograms(const std::vector<cv::Mat> &histograms, int localHistogramsPerDimension, int lineWidth, int histogramHeight)
{
	const int histogramWidth = histograms[0].rows * lineWidth;
	cv::Mat image(histogramHeight * localHistogramsPerDimension, histogramWidth * localHistogramsPerDimension, CV_8U, cv::Scalar(127));

	int i = 0;
	for (int y = 0; y < image.rows; y += histogramHeight) {
		for (int x = 0; x < image.cols; x += histogramWidth) {
			visualizeHistogram(histograms[i], lineWidth, histogramHeight).copyTo(image(cv::Rect(x, y, histogramWidth, histogramHeight)));
			++i;
		}
	}

	return image;
}


/*!
Visualizes a \a histogram using \a lineWidth and \a histogramHeight.
*/
cv::Mat visualizeHistogram(const cv::Mat &histogram, int lineWidth, int histogramHeight)
{
	double minVal = 0.0;
	double maxVal = 0.0;
	cv::minMaxLoc(histogram, &minVal, &maxVal, 0, 0);

	const int histogramWidth = histogram.rows * lineWidth;
	cv::Mat image(histogramHeight, histogramWidth, CV_8U, cv::Scalar(255));

	for (int i = 0; i < histogramWidth; ++i)
		cv::line(image, cv::Point(i, histogramHeight), cv::Point(i, histogramHeight - (int)(histogram.at<float>(i / lineWidth) * histogramHeight / maxVal)), cv::Scalar::all(0));

	return image;
}


/*!
Returns maximum of \a and \b.
*/
inline int maximum(int a, int b)
{
	return (a > b) ? a : b;
}
