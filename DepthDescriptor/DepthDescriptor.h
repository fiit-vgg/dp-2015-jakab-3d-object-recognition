//    File: DepthDescriptor.h
//
//	  Date: April, 2015
//
//  Author: Marek Jakab

#ifndef DEPTHD_H
#define DEPTHD_H

#define NOMINMAX
#include <windows.h>
#include <limits>

#include <opencv2\highgui\highgui.hpp>

#include <Kinect2X.h>



namespace DepthD
{
	class DepthDescriptor
	{
	public:
		DepthDescriptor(float radiusMM);
		~DepthDescriptor();

		void compute(const cv::Mat &depthImage, const cv::Mat &intensityImage, const std::vector<cv::KeyPoint> &colorKeypoints);
		//void compute(const std::vector<cv::Mat> &depthImages, const::std::vector<cv::Mat> &intensityImages, std::vector<std::vector<cv::KeyPoint>> &colorKeypoints);

		void addObjectImpl();
		void recognizeObjectImpl(std::vector<int> &indicesPassedDepth, double distanceRatioThreshold, double minRecognitionConfidence);

		static const int descriptorSize = 4;

	private:
		float radius;
		int global_ind = 0;

		float std_max_flat = 0.15;
		float std_max_flat_nonflat = 0.25;
		float mean_max_flat = 0.2; //0.15;
		float mean_max_flat_nonflat = 0.35; //0.35;

		std::vector<int> flatSurfaceObjectIndices;
		std::vector<int> non_flatSurfaceObjectIndices;

		void storeIndices(int index);
		std::vector<float> mean;
		std::vector<float> std_vector;

		std::vector<cv::KeyPoint> objectDepthKeypoints;
		cv::Mat objectDepthDescriptors;

		cv::Ptr<cv::DescriptorMatcher> descriptorMatcher;

		std::vector<std::vector<cv::KeyPoint>> objectDepthKeypointsVector;
		std::vector<cv::Mat> objectDepthDescriptorsVector;
		std::vector<std::vector<cv::DMatch>> matches;

		void getTriangles(cv::Point keypoint, cv::Point3f &realKeypoint, std::vector<cv::Point> &depthPoints, cv::Mat image);
		void getNormals(std::vector<cv::Point3f> realPoints, float &avgAngle, cv::Vec3f &avgNormal, std::vector<cv::Vec3f> &normals);
		void getStdMaxMin(std::vector<cv::Point3f> realPoints, cv::Point3f realKeypoint, cv::Vec3f avgNormal, float &std, float &maxmin);
		void getAvgAngle(cv::Vec3f avgNormal, cv::Vec3f normal, float &angle);
	};
}

#endif //DEPTHD_H