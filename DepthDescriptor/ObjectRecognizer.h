//    File: ObjectRecognizer.h
//
//	  Date: April, 2015
//
//  Author: Marek Jakab, Marek Racev

#ifndef OBJECTRECOGNIZER_H
#define OBJECTRECOGNIZER_H

#define NOMINMAX
#include <windows.h>
#include <limits>

#include <opencv2/core/core.hpp>
#include <opencv2/features2d/features2d.hpp>
#include <opencv2/core/internal.hpp>
#include <SiftGPU.h>
#include "DepthDescriptor.h"

using namespace DepthD;
using namespace cv;

class ObjectRecognizer : public virtual cv::Algorithm
{
public:
	virtual ~ObjectRecognizer() {};

	virtual bool addObject(const cv::Mat &colorImage, const cv::Mat &depthImage, const cv::Mat &mask);
	virtual int recognizeObject(const cv::Mat &colorImage, const cv::Mat &depthImage, const cv::Mat &mask);
	virtual int getNumberOfObjects();

	virtual bool isInitialized() const;
	virtual void initialize(int cWidth, int cHeight);

	static cv::Ptr<ObjectRecognizer> create(const std::string &recognizerType);
	//static cv::Ptr<ObjectRecognizer> create(const std::string &recognizerType, cv::Ptr<cv::FeatureDetector> featureDetector, cv::Ptr<cv::DescriptorExtractor> descriptorExtractor, cv::Ptr<cv::DescriptorMatcher> descriptorMatcher, cv::Ptr<kcv::KCV_sensor> kcv_sensor, cv::Ptr<SiftGPU> siftGpu = NULL);
	static cv::Ptr<ObjectRecognizer> create(const std::string &recognizerType, cv::Ptr<cv::FeatureDetector> featureDetector, cv::Ptr<cv::DescriptorExtractor> descriptorExtractor, cv::Ptr<cv::DescriptorMatcher> descriptorMatcher, kcv::KCV_sensor *kcv_sensor, cv::Ptr<SiftGPU> siftGpu = NULL);

protected:
	virtual bool addObjectImpl(const cv::Mat &colorImage, const cv::Mat &depthImage, const cv::Mat &mask) = 0;
	virtual int recognizeObjectImpl(const cv::Mat &colorImage, const cv::Mat &depthImage, const cv::Mat &mask) = 0;
	virtual void initializeParameters(int cWidth, int cHeight) { return; };
	virtual int getNumberOfObj() = 0;
};

#endif //OBJECTRECOGNIZER_H
