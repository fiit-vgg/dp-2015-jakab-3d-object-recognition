//    File: SIFTDrecognizer.h
//
//	  Date: April, 2015
//
//  Author: Marek Jakab, Marek Racev

#include "SIFTDrecognizer.h"
#include "common.h"

#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/calib3d/calib3d.hpp>

#include <opencv2\highgui\highgui.hpp>

#define NOMINMAX
#include <windows.h>

#include <limits>
#include "Kinect2X.h"

#include <iostream>
#include <fstream>

/*!
\class SIFTDrecognizer
\brief The SIFTDrecognizer class manages object recognition using RGB-D information from Kinect sensor.
*/

/*!
Constructs a SIFTDrecognizer object.
*/
SIFTDrecognizer::SIFTDrecognizer() :
minKeypointsPerObject(4),
distanceRatioThreshold(0.8),
minRecognitionConfidence(0.1),
minMatchesToFindHomography(4),
ransacOutliersRemovalEnabled(false),
patternSize(15.0f)
{
	SIFTDrecognizer(false);
}

/*!
Constructs a SIFTDrecognizer object.
*/
SIFTDrecognizer::SIFTDrecognizer(bool withDepth) :
minKeypointsPerObject(4),
distanceRatioThreshold(0.8),
minRecognitionConfidence(0.1),
minMatchesToFindHomography(4),
ransacOutliersRemovalEnabled(false),
patternSize(15.0f)
{
	this->depth = withDepth;

	cv::initModule_nonfree();
	featureDetector = cv::FeatureDetector::create("SIFT");
	descriptorExtractor = cv::DescriptorExtractor::create("SIFT");
	descriptorMatcher = cv::DescriptorMatcher::create("BruteForce");

	
}


/*!
Constructs a RMRecognizer object and initializes it with provided \a featureDetector, \a descriptorExtractor, \a descriptorMatcher and \a siftGpu.
*/
SIFTDrecognizer::SIFTDrecognizer(cv::Ptr<cv::FeatureDetector> featureDetector, cv::Ptr<cv::DescriptorExtractor> descriptorExtractor, cv::Ptr<cv::DescriptorMatcher> descriptorMatcher, kcv::KCV_sensor *kcv_sensor, cv::Ptr<SiftGPU> siftGpu) :
featureDetector(featureDetector),
descriptorExtractor(descriptorExtractor),
descriptorMatcher(descriptorMatcher),
siftGpu(siftGpu),
kcv_sensor(kcv_sensor),
minKeypointsPerObject(4),
distanceRatioThreshold(0.8),
minRecognitionConfidence(0.1),
minMatchesToFindHomography(4),
ransacOutliersRemovalEnabled(false),
patternSize(15.0f)
{
}


/*!
Destroys a RMRecognizer object.
*/
SIFTDrecognizer::~SIFTDrecognizer()
{
}

/*!
Initialize the SIFTD recognizer with additional parameters
*/
void SIFTDrecognizer::initializeParameters(int cWidth, int cHeight)
{
	std::string resolution = std::to_string(cWidth) + "x" + std::to_string(cHeight);
	char * cstr = new char[resolution.length() + 1];
	std::strcpy(cstr, resolution.c_str());
	char *argv[] = { "-v", "0", "-p", cstr };
	int argc = sizeof(argv) / sizeof(char *);
	siftGpu = new SiftGPU;
	siftGpu->ParseParam(argc, argv);
	if (siftGpu->VerifyContextGL() != SiftGPU::SIFTGPU_FULL_SUPPORTED) {
		if (siftGpu->CreateContextGL() != SiftGPU::SIFTGPU_FULL_SUPPORTED)
			siftGpu = NULL;
	}

	if (this->depth)
	{
		depthDescriptor = new DepthD::DepthDescriptor(patternSize);
	}
}

/*!
Returns true if RMRecognizer object was successfully initialized.
*/
bool SIFTDrecognizer::isInitialized() const
{
	if ((featureDetector.empty() || descriptorExtractor.empty()) && siftGpu.empty())
		return false;

	if (descriptorMatcher.empty())
		return false;
	return true;
}


/*!
Processes provided \a colorImage, \a depthImage and image \a mask.
*/
void SIFTDrecognizer::processImages(const cv::Mat &colorImage, const cv::Mat &depthImage, const cv::Mat &mask)
{
	// Store object images.
	cv::cvtColor(colorImage, intensityImage, CV_RGBA2GRAY);
	this->depthImage = depthImage;
	this->mask = mask;
	maskedIntensityImage.create(intensityImage.rows, intensityImage.cols, CV_8UC1);
	maskedIntensityImage = cv::Scalar(0);
	intensityImage.copyTo(maskedIntensityImage, mask);

	// Detect features and extract descriptors from object intensity image.
	if (siftGpu.empty()) {
		if (!featureDetector->name().compare("Feature2D.SIFT"))    // Due to bug in OpenCV 2.4.5.
			siftDetect(featureDetector, maskedIntensityImage, objectKeypoints, mask);
		else
			featureDetector->detect(intensityImage, objectKeypoints, mask);

		descriptorExtractor->compute(intensityImage, objectKeypoints, objectDescriptors);
	}
	else {
		runSiftGpu(siftGpu, maskedIntensityImage, objectKeypoints, objectDescriptors, mask);
	}
	
	if (depth)
	{
		this->depthDescriptor->compute(depthImage, maskedIntensityImage, objectKeypoints);
	}
}

int SIFTDrecognizer::getNumberOfObj()
{
		return descriptors.size();
}

/*!
Strores feature descriptors extracted from provided \a colorImage, \a depthImage and image \a mask.
*/
bool SIFTDrecognizer::addObjectImpl(const cv::Mat &colorImage, const cv::Mat &depthImage, const cv::Mat &mask)
{
	processImages(colorImage, depthImage, mask);

	if (objectKeypoints.size() < minKeypointsPerObject)
		return false;

#ifdef _DEBUG
	intensityImages.push_back(intensityImage.clone());
	depthImages.push_back(depthImage.clone());
	masks.push_back(mask.clone());
	maskedIntensityImages.push_back(maskedIntensityImage.clone());
#endif

	keypoints.push_back(objectKeypoints);
	descriptors.push_back(objectDescriptors);
	matches.resize(matches.size() + 1);
	points.resize(points.size() + 1);
	objectPoints.resize(objectPoints.size() + 1);

	if (depth)
		depthDescriptor->addObjectImpl();
	return true;
}


/*!
Returns index of recognized object in provided \a colorImage, aligned \a depthImage and \a mask. If no object was recognized this method returns negative index.
*/
int SIFTDrecognizer::recognizeObjectImpl(const cv::Mat &colorImage, const cv::Mat &depthImage, const cv::Mat &mask)
{
	processImages(colorImage, depthImage, mask);

	int recognizedObjectIndex = -1;
	if (objectKeypoints.size() < minKeypointsPerObject) {
		for (int i = 0; i < matches.size(); ++i)
			matches[i].resize(0);
		return recognizedObjectIndex;
	}

	std::vector<int> indicesPassedDepth;
	if (depth)
	{
		this->depthDescriptor->recognizeObjectImpl(indicesPassedDepth, distanceRatioThreshold, minRecognitionConfidence);
		if (indicesPassedDepth.size() > 0)
		{
			matches.resize(indicesPassedDepth.size());
			std::vector<cv::Mat> descriptorsColor_sorted;
			// select only descriptors which passed trough depth test
			for (int i = 0; i < indicesPassedDepth.size(); i++)
			{
				descriptorsColor_sorted.push_back(descriptors[indicesPassedDepth[i]]);
			}
			robustMatching(descriptorMatcher, objectDescriptors, descriptorsColor_sorted, matches, distanceRatioThreshold);
		}
		else
			return recognizedObjectIndex;
	}else
		robustMatching(descriptorMatcher, objectDescriptors, descriptors, matches, distanceRatioThreshold);

	double maxRecognitionConfidence = minRecognitionConfidence;
	for (int i = 0; i < matches.size(); ++i) {
		int index = (indicesPassedDepth.size() > 0) ? indicesPassedDepth[i] : i;
		if (matches[i].size() < minMatchesToFindHomography)
			continue;

		if (ransacOutliersRemovalEnabled) {
			objectPoints[index].resize(0);
			points[index].resize(0);

			for (int j = 0; j < matches[i].size(); ++j) {
				objectPoints[index].push_back(objectKeypoints[matches[i][j].queryIdx].pt);
				points[index].push_back(keypoints[index][matches[i][j].trainIdx].pt);
			}

			try {
				std::vector<uchar> inliers(matches[i].size(), 0);
				cv::findHomography(objectPoints[index], points[index], inliers, CV_RANSAC);
				extractInlierMatches(matches[i], inliers);
			}
			catch (cv::Exception &e) {}
		}

		double recognitionConfidence = (double)matches[i].size() / ((keypoints[index].size() + objectKeypoints.size()) * 0.5);
		if (maxRecognitionConfidence < recognitionConfidence) {
			maxRecognitionConfidence = recognitionConfidence;
			recognizedObjectIndex = index;
		}
	}
	
	return recognizedObjectIndex;
}
