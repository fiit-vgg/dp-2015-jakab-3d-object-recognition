//    File: nullrecognizer.h
//
//	  Date: April 19, 2013
//
//  Author: Marek Racev

#ifndef NULLRECOGNIZER_H
#define NULLRECOGNIZER_H

#include "objectrecognizer.h"

class NULLrecognizer : public ObjectRecognizer
{
public:
	NULLrecognizer() {};
	~NULLrecognizer() {};

	cv::AlgorithmInfo *info() const;

private:
	void initializeParameters(int cWidth, int cHeight){ return; };
	bool addObjectImpl(const cv::Mat &colorImage, const cv::Mat &depthImage, const cv::Mat &mask) { return true; };
	int recognizeObjectImpl(const cv::Mat &colorImage, const cv::Mat &depthImage, const cv::Mat &mask) { return -1; };
	int getNumberOfObj() { return -1; };

private:
	int nullParam;
};

#endif // NULLRECOGNIZER_H
