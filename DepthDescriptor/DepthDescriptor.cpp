//    File: DepthDescriptor.h
//
//	  Date: April, 2015
//
//  Author: Marek Jakab


#include "DepthDescriptor.h"
#include "common.h"

#include <iostream>
#include <fstream>

using namespace DepthD;

/*!
\class DepthDescriptor
\brief The DepthDescriptor class manages object recognition using D information from Kinect sensor.
*/

/*!
Constructs a DepthDescriptor object.
*/
DepthDescriptor::DepthDescriptor(float radiusMM)
{
	radius = radiusMM / 1000.0; // radius mm in meters
	descriptorMatcher = cv::DescriptorMatcher::create("BruteForce");
}


/*!
Destroys a DepthDescriptor object.
*/
DepthDescriptor::~DepthDescriptor()
{
}

/*!
Extracts depth local feature from \a depthImage and \a intensityImage at specified \a colorKeypoints positions.
*/
void DepthDescriptor::compute(const cv::Mat &depthImage, const cv::Mat &intensityImage, const std::vector<cv::KeyPoint> &colorKeypoints)
{
	if (colorKeypoints.size() == 0) return;
	kcv::KCV_sensor *kcv = kcv::KCV_sensor::getInstance();
	// for training purposes
	kcv->setCoordinateMapper(intensityImage, depthImage);
	objectDepthKeypoints.resize(0);
	objectDepthDescriptors.resize(0);
	mean.clear();
	mean.resize(4);
	std_vector.clear();
	std_vector.resize(4);

	if (intensityImage.size() == depthImage.size()) {
		this->objectDepthKeypoints = colorKeypoints;
	}
	else {
		cv::Point point; //Point2f
		for (int i = 0; i < colorKeypoints.size(); ++i) {
			if (kcv->getPointInDepth(colorKeypoints[i].pt, intensityImage.cols, intensityImage.rows,
				depthImage.cols, depthImage.rows, point))
			objectDepthKeypoints.push_back(cv::KeyPoint(point,0));
		}
	}
	
	this->objectDepthDescriptors.create(objectDepthKeypoints.size(), 4, CV_32FC1);

	float *descriptor;

	std::vector<cv::Vec3f> avg_normals;
	cv::Vec3f object_normal;
	int good_normals = 0;
	object_normal[0] = 0.0f;
	object_normal[1] = 0.0f;
	object_normal[2] = 0.0f;

	float max_angle = 0.0f, max_std = 0.0f, max_maxmin = 0.0f;

	int lastBadDescriptorIndex = -1;
	std::vector<cv::KeyPoint> objectDepthKeypointsGood;

	for (int i = 0; i < objectDepthKeypoints.size(); ++i)
	{
		descriptor = objectDepthDescriptors.ptr<float>(i);
		std::vector<cv::Point> depthPoints;
		std::vector<cv::Point3f> realPoints;
		cv::Point3f realKeypoint;

		// get Triangles and extract points
		getTriangles(cv::Point((int)objectDepthKeypoints[i].pt.x, (int)objectDepthKeypoints[i].pt.y), realKeypoint, depthPoints, depthImage);
		for (int j = 0; j < depthPoints.size(); ++j)
		{
			
			cv::Point3f realPoint1, realPoint2, realPoint3;
			// get real world coordinates
			if (kcv->getPointInReal(depthPoints[j], depthImage.cols, depthImage.rows, realPoint1))
			{
				if (kcv->getPointInReal(depthPoints[j + 1], depthImage.cols, depthImage.rows, realPoint2))
				{
					if (kcv->getPointInReal(depthPoints[j + 2], depthImage.cols, depthImage.rows, realPoint3))
					{
						// store real coordinates
						realPoints.push_back(realPoint1);
						realPoints.push_back(realPoint2);
						realPoints.push_back(realPoint3);
					}
				}
			}
			j = j + 2;
		}
		cv::Vec3f avgNormal;
		std::vector<cv::Vec3f> normals;
		float avgAngle;
		// initialization to zero to avoid infinity values
		if (realPoints.size() == 0)
		{
			descriptor[0] = 0.0f;
			descriptor[1] = 0.0f;
			descriptor[2] = 0.0f;
			if (lastBadDescriptorIndex < 0)
			{
				lastBadDescriptorIndex = i;
				continue;
			}
			continue;
		}
		// get average normal and first feature -- average angle
		getNormals(realPoints, avgAngle, avgNormal, normals);
		
		float std, maxmin;
		// standard deviation and max-min
		// we translate surface to the keypoint position
		getStdMaxMin(realPoints, realKeypoint, avgNormal, std, maxmin);
		// store values for later normalisation
		if (!(avgAngle == avgAngle))
		{
			descriptor[0] = 0.0f;
			if (lastBadDescriptorIndex < 0)
			{
				lastBadDescriptorIndex = i;
				continue;
			}
			continue;
		}
			
		if (!(std == std))
		{
			descriptor[1] = 0.0f;
			if (lastBadDescriptorIndex < 0)
			{
				lastBadDescriptorIndex = i;
				continue;
			}
			continue;
		}
		if (!(maxmin == maxmin))
		{
			descriptor[2] = 0.0f;
			if (lastBadDescriptorIndex < 0)
			{
				lastBadDescriptorIndex = i;
				continue;
			}
			continue;
		}

		if ((avgNormal[0] == avgNormal[0]) && (avgNormal[1] == avgNormal[1]) && (avgNormal[2] == avgNormal[2]))
		{
			good_normals++;
			object_normal[0] = object_normal[0] + avgNormal[0];
			object_normal[1] = object_normal[1] + avgNormal[1];
			object_normal[2] = object_normal[2] + avgNormal[2];
		}

		// clear all
		realPoints.clear();
		depthPoints.clear();
		if (avgAngle > max_angle) 
			max_angle = avgAngle;
		descriptor[0] = avgAngle;
		if (std > max_std) 
			max_std = std;
		descriptor[1] = std;
		if (maxmin > max_maxmin) 
			max_maxmin = maxmin;
		descriptor[2] = maxmin;

		avg_normals.push_back(avgNormal);
		objectDepthKeypointsGood.push_back(objectDepthKeypoints[i]);
		if (lastBadDescriptorIndex < 0)		
			continue;
		std::memcpy(objectDepthDescriptors.ptr<float>(lastBadDescriptorIndex), objectDepthDescriptors.ptr<float>(i), 4 * sizeof(float));
		++lastBadDescriptorIndex;
	}

	objectDepthKeypoints = objectDepthKeypointsGood;
	objectDepthKeypointsGood.clear();
	if (lastBadDescriptorIndex >= 0)
		objectDepthDescriptors.resize(lastBadDescriptorIndex);
	lastBadDescriptorIndex = -1;

	object_normal[0] = object_normal[0] / (float)good_normals;
	object_normal[1] = object_normal[1] / (float)good_normals;
	object_normal[2] = object_normal[2] / (float)good_normals;
	float max_object_angle = 0.0f;

	std::vector<std::vector<float>> descriptorValues;
	descriptorValues.resize(4);
	

	for (int i = 0; i < avg_normals.size(); i++)
	{
		// if average normal is finite & with no error
		descriptor = objectDepthDescriptors.ptr<float>(i);
		if ((avg_normals[i][0] == avg_normals[i][0]) &&
			(avg_normals[i][1] == avg_normals[i][1]) &&
			(avg_normals[i][2] == avg_normals[i][2]))
		{
			float angle;
			// compute global angle
			getAvgAngle(object_normal, avg_normals[i], angle);
			if (!(angle == angle))
			{
				descriptor[3] = 0.0f;
				if (lastBadDescriptorIndex < 0)
				{
					lastBadDescriptorIndex = i;
					continue;
				}
				continue;
			}
			descriptor[3] = angle;
			if (angle > max_object_angle)
				max_object_angle = angle;
		}
		else
		{
			descriptor[3] = 0.0f;
			if (lastBadDescriptorIndex < 0)
			{
				lastBadDescriptorIndex = i;
				continue;
			}
			continue;
		}
		objectDepthKeypointsGood.push_back(objectDepthKeypoints[i]);
		for (int j = 0; j < 4; j++)
			descriptorValues[j].push_back(descriptor[j]);
		if (lastBadDescriptorIndex < 0)
			continue;
		std::memcpy(objectDepthDescriptors.ptr<float>(lastBadDescriptorIndex), objectDepthDescriptors.ptr<float>(i), 4 * sizeof(float));
		++lastBadDescriptorIndex;
	}
	objectDepthKeypoints = objectDepthKeypointsGood;
	objectDepthKeypointsGood.clear();
	if (lastBadDescriptorIndex >= 0)
		objectDepthDescriptors.resize(lastBadDescriptorIndex);

	if (objectDepthKeypoints.size() < 4)
		return;

	for (int j = 0; j < 4; j++)
		std::sort(descriptorValues[j].begin(), descriptorValues[j].end());

	float upper_quartile[4];
	float lower_quartile[4];
	float out_fence[4];
	for (int j = 0; j < 4; j++)
	{
		upper_quartile[j] = descriptorValues[j][(int)(descriptorValues[j].size()*(3.0 / 4.0))];
		lower_quartile[j] = descriptorValues[j][(int)(descriptorValues[j].size()*(1.0 / 4.0))];
		out_fence[j] = (upper_quartile[j] - lower_quartile[j])*1.5; //3.0
	}

	lastBadDescriptorIndex = -1;

	for (int i = 0; i < objectDepthDescriptors.rows; i++)
	{
		descriptor = objectDepthDescriptors.ptr<float>(i);
		bool isOK = true;
		for (int j = 0; j < 4; j++)
		{
			if (descriptor[j] < lower_quartile[j] - out_fence[j])
			{
				if (lastBadDescriptorIndex < 0)
				{
					lastBadDescriptorIndex = i;
					isOK = false;
					continue;
				}
				isOK = false;
				continue;
			}
			if (descriptor[j] > upper_quartile[j] + out_fence[j])
			{
				if (lastBadDescriptorIndex < 0)
				{
					lastBadDescriptorIndex = i;
					isOK = false;
					continue;
				}
				isOK = false;
				continue;
			}
		}
		if (!isOK)
			continue;
		objectDepthKeypointsGood.push_back(objectDepthKeypoints[i]);
		for (int j = 0; j < 4; j++)
			mean[j] += descriptor[j];
		if (lastBadDescriptorIndex < 0)
			continue;
		std::memcpy(objectDepthDescriptors.ptr<float>(lastBadDescriptorIndex), objectDepthDescriptors.ptr<float>(i), 4 * sizeof(float));
		++lastBadDescriptorIndex;
	}
	objectDepthKeypoints = objectDepthKeypointsGood;
	objectDepthKeypointsGood.clear();
	if (lastBadDescriptorIndex >= 0)
		objectDepthDescriptors.resize(lastBadDescriptorIndex);
	for (int j = 0; j < 4; j++)
		mean[j] = mean[j] / objectDepthDescriptors.rows;

	for (int i = 0; i < objectDepthDescriptors.rows; i++)
	{
		// if average normal is finite & with no error
		descriptor = objectDepthDescriptors.ptr<float>(i);
		for (int j = 0; j < 4; j++)
			std_vector[j] += std::powf((descriptor[j] - mean[j]), 2);
	}
	for (int j = 0; j < 4; j++)
		std_vector[j] = std::sqrtf(std_vector[j] / objectDepthDescriptors.rows);

	avg_normals.clear();
	// normalisation
	//for (int i = 0; i < objectDepthKeypoints.size(); ++i)
	//{
	//	descriptor = objectDepthDescriptors.ptr<float>(i);
	//	descriptor[0] = descriptor[0] / max_angle; //x
	//	if (!(descriptor[0] == descriptor[0])) descriptor[0] = 0.0f;
	//	descriptor[1] = descriptor[1] / max_std;
	//	if (!(descriptor[1] == descriptor[1])) descriptor[1] = 0.0f;
	//	descriptor[2] = descriptor[2] / max_maxmin;
	//	if (!(descriptor[2] == descriptor[2])) descriptor[2] = 0.0f;
	//	descriptor[3] = descriptor[3] / max_object_angle;
	//	if (!(descriptor[3] == descriptor[3])) descriptor[3] = 0.0f;
	//}
}

/*!
Store depth local features.
*/
void DepthDescriptor::addObjectImpl()
{
	global_ind++;
	objectDepthKeypointsVector.push_back(objectDepthKeypoints);
	objectDepthDescriptorsVector.push_back(objectDepthDescriptors);
	matches.resize(matches.size() + 1);

	storeIndices(objectDepthDescriptorsVector.size() - 1);
}

/*!
Strores \a index of flat or non-flat object in separated vector.
*/
void DepthDescriptor::storeIndices(int index)
{
	if (mean[3] < mean_max_flat)//isFlat())
	{
		flatSurfaceObjectIndices.push_back(index);
	} else
		non_flatSurfaceObjectIndices.push_back(index);
}

/*!
Returns \a indicesPassedDepth vector of recognized objects with set \a distanceRatioThreshold and \a minRecognitionConfidence for matcher.
*/
void DepthDescriptor::recognizeObjectImpl(std::vector<int> &indicesPassedDepth, double distanceRatioThreshold, double minRecognitionConfidence)
{
	if (objectDepthKeypoints.size() < 4) {
		for (int i = 0; i < matches.size(); ++i)
			matches[i].resize(0);
		return;
	}
	indicesPassedDepth.clear();
	if (mean[3] < mean_max_flat)
	{
		std::vector<int> vector(flatSurfaceObjectIndices);
		indicesPassedDepth = vector;
		return;
	}
	else if(mean[3] < mean_max_flat_nonflat)
	{
		std::vector<int> vector(flatSurfaceObjectIndices);
		indicesPassedDepth = vector;
	}

	matches.resize(non_flatSurfaceObjectIndices.size());
	double maxRecognitionConfidence = minRecognitionConfidence;
	for (int i = 0; i < matches.size(); ++i)
	{
		robustMatching(descriptorMatcher, objectDepthDescriptors, objectDepthDescriptorsVector[non_flatSurfaceObjectIndices[i]], matches[i], 0.85); //0.85 pre sift
		if (matches[i].size() < 4)
			continue;
		double recognitionConfidence = (double)matches[i].size() / ((objectDepthKeypointsVector[non_flatSurfaceObjectIndices[i]].size() + objectDepthKeypoints.size()) * 0.5);
		if (maxRecognitionConfidence < recognitionConfidence + 0.05) { // threshold for similar depth objects
			if (maxRecognitionConfidence < recognitionConfidence)
				maxRecognitionConfidence = recognitionConfidence;
			indicesPassedDepth.push_back(non_flatSurfaceObjectIndices[i]);
		}
	}

}

/*!
Extract Triangle coordinates from provided \a keypoint and \a image .Store values in a \a realKeypoint and \a depthPoints
*/
void DepthDescriptor::getTriangles(cv::Point keypoint, cv::Point3f &realKeypoint, std::vector<cv::Point> &depthPoints, cv::Mat image)
{
	cv::Point3f world;
	kcv::KCV_sensor *kcv = kcv::KCV_sensor::getInstance();

	// counter clockwise 
	if (kcv->getPointInReal(keypoint, image.cols, image.rows, world))
	{
		realKeypoint = world;
		cv::Point3f world1, world2, world3;
		cv::Point depthPoint1, depthPoint2, depthPoint3;
		world1.x = world.x - sqrt(radius*radius + radius*radius) / 2;
		world1.y = world.y - sqrt(radius*radius + radius*radius) / 2;
		world1.z = world.z;

		world2.x = world.x + sqrt(radius*radius + radius*radius) / 2;
		world2.y = world.y - sqrt(radius*radius + radius*radius) / 2;
		world2.z = world.z;

		world3.x = world.x;
		world3.y = world.y + radius;
		world3.z = world.z;

		kcv->getPointFromReal(world1, image.cols, image.rows, depthPoint1);
		kcv->getPointFromReal(world2, image.cols, image.rows, depthPoint2);
		kcv->getPointFromReal(world3, image.cols, image.rows, depthPoint3);

		depthPoints.push_back(depthPoint1); depthPoints.push_back(depthPoint2); depthPoints.push_back(depthPoint3);

		world1.x = world.x - sqrt(radius*radius + radius*radius) / 2;
		world1.y = world.y + sqrt(radius*radius + radius*radius) / 2;
		world1.z = world.z;

		world2.x = world.x;
		world2.y = world.y - radius;
		world2.z = world.z;

		world3.x = world.x + sqrt(radius*radius + radius*radius) / 2;
		world3.y = world.y + sqrt(radius*radius + radius*radius) / 2;
		world3.z = world.z;

		kcv->getPointFromReal(world1, image.cols, image.rows, depthPoint1);
		kcv->getPointFromReal(world2, image.cols, image.rows, depthPoint2);
		kcv->getPointFromReal(world3, image.cols, image.rows, depthPoint3);

		depthPoints.push_back(depthPoint1); depthPoints.push_back(depthPoint2); depthPoints.push_back(depthPoint3);

		world1.x = world.x - sqrt(radius*radius + radius*radius) / 2;
		world1.y = world.y + sqrt(radius*radius + radius*radius) / 2;
		world1.z = world.z;

		world2.x = world.x - sqrt(radius*radius + radius*radius) / 2;
		world2.y = world.y - sqrt(radius*radius + radius*radius) / 2;
		world2.z = world.z;

		world3.x = world.x + radius;
		world3.y = world.y;
		world3.z = world.z;

		kcv->getPointFromReal(world1, image.cols, image.rows, depthPoint1);
		kcv->getPointFromReal(world2, image.cols, image.rows, depthPoint2);
		kcv->getPointFromReal(world3, image.cols, image.rows, depthPoint3);

		depthPoints.push_back(depthPoint1); depthPoints.push_back(depthPoint2); depthPoints.push_back(depthPoint3);

		world1.x = world.x + sqrt(radius*radius + radius*radius) / 2;
		world1.y = world.y + sqrt(radius*radius + radius*radius) / 2;
		world1.z = world.z;

		world2.x = world.x - radius;
		world2.y = world.y;
		world2.z = world.z;

		world3.x = world.x + sqrt(radius*radius + radius*radius) / 2;
		world3.y = world.y - sqrt(radius*radius + radius*radius) / 2;
		world3.z = world.z;

		kcv->getPointFromReal(world1, image.cols, image.rows, depthPoint1);
		kcv->getPointFromReal(world2, image.cols, image.rows, depthPoint2);
		kcv->getPointFromReal(world3, image.cols, image.rows, depthPoint3);

		depthPoints.push_back(depthPoint1); depthPoints.push_back(depthPoint2); depthPoints.push_back(depthPoint3);
	}
}
/*!
Extract \a avgAngle , \a avgNormal and \a normals vectors from provided \a realPoints.
*/
void DepthDescriptor::getNormals(std::vector<cv::Point3f> realPoints, float &avgAngle, cv::Vec3f &avgNormal, std::vector<cv::Vec3f> &normals)
{
	for (int i = 0; i < realPoints.size(); ++i)
	{
		cv::Vec3f diff = realPoints[i + 1] - realPoints[i];
		cv::Vec3f diff2 = realPoints[i + 2] - realPoints[i];
		cv::Vec3f normal = diff.cross(diff2);
		normal = cv::normalize(normal);
		
		normals.push_back(normal);

		i = i + 2;
	}
	float x = 0.0f;
	float y = 0.0f;
	float z = 0.0f;

	for (int i = 0; i < normals.size(); i++)
	{
		x = x + normals[i][0];
		y = y + normals[i][1];
		z = z + normals[i][2];
	}
	avgNormal[0] = x / normals.size();
	avgNormal[1] = y / normals.size();
	avgNormal[2] = z / normals.size();

	avgAngle = 0.0f;
	for (int i = 0; i < normals.size(); i++)
	{
		float t = (avgNormal[0] * normals[i][0] + avgNormal[1] * normals[i][1] + avgNormal[2] * normals[i][2]) /
			(std::sqrtf(std::powf(avgNormal[0], 2) + std::powf(avgNormal[1], 2) + std::powf(avgNormal[2], 2)) *
			std::sqrtf(std::powf(normals[i][0], 2) + std::powf(normals[i][1], 2) + std::powf(normals[i][2], 2)));
		float test = std::acosf(t);
		avgAngle += test;
	}
	avgAngle = avgAngle / normals.size();
}
/*!
Extract \a angle between two vectors \a avgNormal and \a normal.
*/
void DepthDescriptor::getAvgAngle(cv::Vec3f avgNormal, cv::Vec3f normal, float &angle)
{
	float t = (avgNormal[0] * normal[0] + avgNormal[1] * normal[1] + avgNormal[2] * normal[2]) /
		(std::sqrtf(std::powf(avgNormal[0], 2) + std::powf(avgNormal[1], 2) + std::powf(avgNormal[2], 2)) *
		std::sqrtf(std::powf(normal[0], 2) + std::powf(normal[1], 2) + std::powf(normal[2], 2)));
	angle = std::acosf(t);
}
/*!
Extract Standard deviation \a std and max - min  \a maxmin values from \a realPoints , \a realKeypoint and \a avgNormal.
*/
void DepthDescriptor::getStdMaxMin(std::vector<cv::Point3f> realPoints, cv::Point3f realKeypoint, cv::Vec3f avgNormal, float &std, float &maxmin)
{
	float avg = 0.0f;
	float max = std::numeric_limits<float>::min();
	float min = std::numeric_limits<float>::max();
	std::vector<float> distances;
	for (int i = 0; i < realPoints.size(); i++)
	{
		// as we know normal vector and keypoint real coordinates we can calculate distance between points
		// create vector from the plane
		cv::Vec3f w(realKeypoint.x - realPoints[i].x, realKeypoint.y - realPoints[i].y, realKeypoint.z - realPoints[i].z);
		// project w onto avgNormal to get distance
		float dist = (avgNormal[0] * w[0] + avgNormal[1] * w[1] + avgNormal[2] * w[2])
			/ std::sqrtf(std::powf(avgNormal[0], 2) + std::powf(avgNormal[1], 2) + std::powf(avgNormal[2], 2));
		distances.push_back(dist);
		if (dist > max)
			max = dist;
		if (dist < min)
			min = dist;
		avg += abs(dist);
	}
	maxmin = max - min;
	avg = avg / realPoints.size();
	float sum_diff_squared = 0.0f;
	for (int i = 0; i < distances.size(); i++)
	{
		sum_diff_squared += powf((avg - distances[i]), 2);
	}
	std = sqrtf((sum_diff_squared / distances.size()));
	distances.clear();
}