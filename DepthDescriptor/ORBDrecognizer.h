//    File: ORBDrecognizer.h
//
//	  Date: April, 2015
//
//  Author: Marek Jakab

#include "objectrecognizer.h"

#include <opencv2\gpu\gpu.hpp>
#include <opencv2\stitching\detail\matchers.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/features2d/features2d.hpp>
#include <opencv2/nonfree/features2d.hpp>
#include <opencv2/nonfree/nonfree.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2\highgui\highgui.hpp>

#define NOMINMAX
#include <windows.h>

#include <limits>
#include "Kinect2X.h"

#include <iostream>
#include <fstream>

#include "depthdescriptor.h"

class ORBDrecognizer : public ObjectRecognizer
{

public:
	ORBDrecognizer();
	ORBDrecognizer(bool withDepth);
	~ORBDrecognizer();

	
	bool isInitialized() const;

	cv::AlgorithmInfo *info() const;

private:
	void initializeParameters(int cWidth, int cHeight);
	bool addObjectImpl(const cv::Mat &colorImage, const cv::Mat &depthImage, const cv::Mat &mask);
	int recognizeObjectImpl(const cv::Mat &colorImage, const cv::Mat &depthImage, const cv::Mat &mask);
	int getNumberOfObj();

private:
	void processImages(const cv::Mat &colorImage, const cv::Mat &depthImage, const cv::Mat &mask);

private:

	bool depth = false;

	DepthD::DepthDescriptor *depthDescriptor = NULL;

	std::vector<gpu::GpuMat> intensityImages;
	std::vector<gpu::GpuMat> depthImages;
	std::vector<gpu::GpuMat> masks;
	std::vector<gpu::GpuMat> maskedIntensityImages;

	cv::gpu::GpuMat intensityImage;
	cv::gpu::GpuMat depthImage;
	cv::gpu::GpuMat mask;
	cv::gpu::GpuMat maskedIntensityImage;

	cv::gpu::GpuMat objectDescriptors;
	cv::gpu::GpuMat objectKeypoints;
	std::vector<std::vector<cv::DMatch>> matches;

	std::vector<cv::gpu::GpuMat> keypoints;

	std::vector<gpu::GpuMat> descriptors;
	std::vector<int> indicesPassedDepth;

	std::vector<std::vector<cv::Point2f>> points;
	std::vector<std::vector<cv::Point2f>> objectPoints;


	cv::gpu::ORB_GPU orbGpu;
	kcv::KCV_sensor *kcv_sensor;

	int minKeypointsPerObject;
	double distanceRatioThreshold;
	double minRecognitionConfidence;
	int minMatchesToFindHomography;
	bool ransacOutliersRemovalEnabled;
	float patternSize;

};