//    File: ORBDrecognizer.h
//
//	  Date: April, 2015
//
//  Author: Marek Jakab

#include "ORBDrecognizer.h"
#include "DepthDescriptor.h"
#include "common.h"

/*!
\class ORBDrecognizer
\brief The ORBDrecognizer class manages object recognition using RGB-D information from Kinect sensor.
*/

/*!
Constructs a ORBDrecognizer object.
*/
ORBDrecognizer::ORBDrecognizer() :
minKeypointsPerObject(4),
distanceRatioThreshold(0.8),
minRecognitionConfidence(0.1),
minMatchesToFindHomography(4),
ransacOutliersRemovalEnabled(false),
patternSize(15.0f)
{
	cv::initModule_nonfree();
	this->depth = false;
}

/*!
Constructs a ORBDrecognizer object.
*/
ORBDrecognizer::ORBDrecognizer(bool withDepth) :
minKeypointsPerObject(4),
distanceRatioThreshold(0.8),
minRecognitionConfidence(0.1),
minMatchesToFindHomography(4),
ransacOutliersRemovalEnabled(false),
patternSize(15.0f)
{

	cv::initModule_nonfree();
	this->depth = withDepth;
	
}

/*!
Destroys a ORBDrecognizer object.
*/
ORBDrecognizer::~ORBDrecognizer()
{
}


/*!
Returns true if ORBDRecognizer object was successfully initialized.
*/
bool ORBDrecognizer::isInitialized() const
{
	int ngpus = cv::gpu::getCudaEnabledDeviceCount();
	if (ngpus < 1) return false;
	return true;
}

/*!
Initialize the ORBD recognizer with additional parameters
*/
void ORBDrecognizer::initializeParameters(int cWidth, int cHeight)
{
	if (this->depth)
	{
		depthDescriptor = new DepthD::DepthDescriptor(patternSize);
	}
}

/*!
Processes provided \a colorImage, \a depthImage and image \a mask.
*/
void ORBDrecognizer::processImages(const cv::Mat &colorImage, const cv::Mat &depthImage, const cv::Mat &mask)
{
	// Store object images.
	cv::Mat intensityImage;
	cv::cvtColor(colorImage, intensityImage, CV_RGBA2GRAY);
	this->intensityImage.upload(intensityImage);
	this->depthImage.upload(depthImage);
	this->mask.upload(mask);
	maskedIntensityImage.create(this->intensityImage.rows, this->intensityImage.cols, CV_8UC1);
	maskedIntensityImage = cv::Scalar(0);
	this->intensityImage.copyTo(maskedIntensityImage, this->mask);

	// Detect features and extract descriptors from object intensity image.
	cv::gpu::ORB_GPU orbGpu(100);
	this->orbGpu = orbGpu;
	runOrbGpu(this->orbGpu, this->intensityImage, this->mask, objectKeypoints, objectDescriptors); //maskedIntensityImage

	if (depth)
	{
		cv::Mat maskedIntensityImageCPU;
		std::vector<cv::KeyPoint> objectKeypointsCPU;
		maskedIntensityImage.download(maskedIntensityImageCPU);
		orbGpu.downloadKeyPoints(objectKeypoints, objectKeypointsCPU);
		this->depthDescriptor->compute(depthImage, maskedIntensityImageCPU, objectKeypointsCPU);
	}
}

/*!
Returns number of trained objects.
*/
int ORBDrecognizer::getNumberOfObj()
{
		return descriptors.size();
}

/*!
Strores feature descriptors extracted from provided \a colorImage, \a depthImage and image \a mask.
*/
bool ORBDrecognizer::addObjectImpl(const cv::Mat &colorImage, const cv::Mat &depthImage, const cv::Mat &mask)
{
	processImages(colorImage, depthImage, mask);

	if (objectKeypoints.rows < minKeypointsPerObject)
		return false;

#ifdef _DEBUG
	intensityImages.push_back(this->intensityImage.clone());
	depthImages.push_back(this->depthImage.clone());
	masks.push_back(this->mask.clone());
	maskedIntensityImages.push_back(this->maskedIntensityImage.clone());
#endif

	keypoints.push_back(objectKeypoints.clone());
	descriptors.push_back(objectDescriptors.clone());
	matches.resize(matches.size() + 1);
	points.resize(points.size() + 1);
	objectPoints.resize(objectPoints.size() + 1);

	if (depth)
		depthDescriptor->addObjectImpl();

	return true;
}


/*!
Returns index of recognized object in provided \a colorImage, aligned \a depthImage and \a mask. If no object was recognized this method returns negative index.
*/
int ORBDrecognizer::recognizeObjectImpl(const cv::Mat &colorImage, const cv::Mat &depthImage, const cv::Mat &mask)
{
	processImages(colorImage, depthImage, mask);

	int recognizedObjectIndex = -1;
	if (objectKeypoints.cols < minKeypointsPerObject) {
		for (int i = 0; i < matches.size(); ++i)
			matches[i].resize(0);
		return recognizedObjectIndex;
	}

	cv::gpu::BruteForceMatcher_GPU<Hamming> descriptorMatcher;
	cv::Ptr<cv::DescriptorMatcher> matcher = cv::DescriptorMatcher::create("BruteForce");

	std::vector<int> indicesPassedDepth;
	if (depth)
	{
		this->depthDescriptor->recognizeObjectImpl(indicesPassedDepth, distanceRatioThreshold, minRecognitionConfidence);
		if (indicesPassedDepth.size() > 0)
		{
			matches.resize(indicesPassedDepth.size());
			// select only descriptors which passed trough depth test
			std::vector<cv::gpu::GpuMat> descriptorsColor_sortedGPU;
			for (int i = 0; i < indicesPassedDepth.size(); i++)
			{
				descriptorsColor_sortedGPU.push_back(descriptors[indicesPassedDepth[i]]);
			}
			double maxRecognitionConfidence = minRecognitionConfidence;
			for (int i = 0; i < descriptorsColor_sortedGPU.size(); ++i)
			{
				descriptorMatcher.knnMatch(objectDescriptors, descriptorsColor_sortedGPU[i], matches, 2);

				if (matches.size() < minMatchesToFindHomography)
					continue;

				std::vector< DMatch > good_matches;
				int min_n;
				if ((objectDescriptors.rows - 1) < (int)matches.size())
					min_n = objectDescriptors.rows - 1;
				else
					min_n = (int)matches.size();
				for (int k = 0; k < min_n; k++)
				{
					if ((matches[k][0].distance < 0.85*(matches[k][1].distance)) && ((int)matches[k].size() <= 2 && (int)matches[k].size()>0))
					{
						good_matches.push_back(matches[k][0]);
					}
				}

				double recognitionConfidence = (double)good_matches.size() / ((keypoints[indicesPassedDepth[i]].cols + objectKeypoints.cols) * 0.5);
				if (maxRecognitionConfidence < recognitionConfidence) {
					maxRecognitionConfidence = recognitionConfidence;
					recognizedObjectIndex = indicesPassedDepth[i];
				}
			}
		}
		else
			return recognizedObjectIndex;
	}
	else
	{
		double maxRecognitionConfidence = minRecognitionConfidence;
		for (int i = 0; i < descriptors.size(); ++i)
		{
			descriptorMatcher.knnMatch(objectDescriptors, descriptors[i], matches, 2);

			if (matches.size() < minMatchesToFindHomography)
				continue;

			std::vector< DMatch > good_matches;
			int min_n;
			if ((objectDescriptors.rows - 1) < (int)matches.size())
				min_n = objectDescriptors.rows - 1;
			else
				min_n = (int)matches.size();
			for (int k = 0; k < min_n; k++)
			{
				if ((matches[k][0].distance < 0.85*(matches[k][1].distance)) && ((int)matches[k].size() <= 2 && (int)matches[k].size()>0))
				{
					good_matches.push_back(matches[k][0]);
				}
			}

			double recognitionConfidence = (double)good_matches.size() / ((keypoints[i].cols + objectKeypoints.cols) * 0.5);
			if (maxRecognitionConfidence < recognitionConfidence) {
				maxRecognitionConfidence = recognitionConfidence;
				recognizedObjectIndex = i;
			}
		}
	}

	return recognizedObjectIndex;
}
