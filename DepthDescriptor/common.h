//    File: common.h
//
//	  Date: April, 2015
//
//  Author: Marek Jakab, Marek Racev

#ifndef COMMON_H
#define COMMON_H

#include <opencv2/core/core.hpp>
#include <opencv2/features2d/features2d.hpp>
#include <opencv2/gpu/gpu.hpp>
#include <SiftGPU.h>

void runSiftGpu(cv::Ptr<SiftGPU> &siftGpu, const cv::Mat &image, std::vector<cv::KeyPoint> &keypoints, cv::Mat &descriptors, const cv::Mat &mask);
void runOrbGpu(cv::gpu::ORB_GPU &orbGpu, const cv::gpu::GpuMat &image, const cv::gpu::GpuMat &mask, cv::gpu::GpuMat &keypoints, cv::gpu::GpuMat &descriptors);
void runFastDepthKP(const cv::gpu::GpuMat &depthImage, const cv::gpu::GpuMat &mask, std::vector<cv::KeyPoint> &keypoints, int threshold);
void runFastDepthKP(const cv::Mat &depthImage, const cv::Mat &mask, std::vector<cv::KeyPoint> &keypoints, int threshold);

void convertKeypoints(const SiftGPU::SiftKeypoint siftKeypoints[], int numKeypoints, std::vector<cv::KeyPoint> &cvKeypoints);
void siftDetect(cv::Ptr<cv::FeatureDetector> &featureDetector, const cv::Mat &image, std::vector<cv::KeyPoint>& keypoints, const cv::Mat &mask);

void robustMatching(cv::Ptr<cv::DescriptorMatcher> &descriptorMatcher, const cv::Mat &queryDescriptors, const std::vector<cv::Mat> &trainDescriptors, std::vector<std::vector<cv::DMatch>> &matches, double ratioThreshold = 0.8);
void robustMatching(cv::Ptr<cv::DescriptorMatcher> &descriptorMatcher, const cv::Mat &queryDescriptors, const cv::Mat &trainDescriptors, std::vector<cv::DMatch> &matches, double ratioThreshold = 0.8);
void distanceRatioFilter(std::vector<std::vector<cv::DMatch>> &knnMatches, double ratioThreshold);
void crossCheckFilter(const std::vector<std::vector<cv::DMatch>> &knnMatches1, const std::vector<std::vector<cv::DMatch>> &knnMatches2, std::vector<cv::DMatch> &matches);

void unidirectionalMatching(cv::Ptr<cv::DescriptorMatcher> &descriptorMatcher, const cv::Mat &queryDescriptors, const std::vector<cv::Mat> &trainDescriptors, std::vector<std::vector<cv::DMatch>> &matches, double ratioThreshold);
void unidirectionalMatching(cv::Ptr<cv::DescriptorMatcher> &descriptorMatcher, const cv::Mat &queryDescriptors, const cv::Mat &trainDescriptors, std::vector<cv::DMatch> &matches, double ratioThreshold);
void distanceRatioFilter(const std::vector<std::vector<cv::DMatch>> &knnMatches, std::vector<cv::DMatch> &matches, double ratioThreshold);

void simpleMatching(cv::Ptr<cv::DescriptorMatcher> &descriptorMatcher, const cv::Mat &queryDescriptors, const std::vector<cv::Mat> &trainDescriptors, std::vector<std::vector<cv::DMatch>> &matches);
void filterGoodMatches(std::vector<cv::DMatch> &matches, double maxDescriptorDistanceFactor, double minDescriptorDistanceThreshold);
int filterGoodMatches(std::vector<cv::DMatch> &matches, double minDescriptorDistanceThreshold);
void extractInlierMatches(std::vector<cv::DMatch> &matches, const std::vector<uchar> &inliers);

void computeLocalHistograms(const cv::Mat &image, std::vector<cv::Mat> &localHistograms, int localHistogramsPerDimension = 10, int localHistogramsSize = 16);
double computeObjectSimilarity(const cv::Mat &image, const std::vector<cv::Mat> &localHistograms, double objectSimilarityThreshold, int localHistogramsPerDimension = 10, int localHistogramsSize = 16);
cv::Mat visualizeHistograms(const std::vector<cv::Mat> &histograms, int localHistogramsPerDimension, int lineWidth, int histogramHeight);
cv::Mat visualizeHistogram(const cv::Mat &histogram, int lineWidth, int histogramHeight);

inline int maximum(int a, int b);

#endif // COMMON_H
