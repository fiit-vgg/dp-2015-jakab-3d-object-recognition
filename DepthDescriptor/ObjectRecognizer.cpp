//    File: ObjectRecognizer.h
//
//	  Date: April, 2015
//
//  Author: Marek Jakab, Marek Racev

#include "ObjectRecognizer.h"
#include "SIFTDrecognizer.h"
#include "ORBDrecognizer.h"
#include "nullrecognizer.h"

CV_INIT_ALGORITHM(SIFTDrecognizer, "ObjectRecognition.SIFTD",
	obj.info()->addParam(obj, "minKeypointsPerObject", obj.minKeypointsPerObject);
obj.info()->addParam(obj, "distanceRatioThreshold", obj.distanceRatioThreshold);
obj.info()->addParam(obj, "minRecognitionConfidence", obj.minRecognitionConfidence);
obj.info()->addParam(obj, "minMatchesToFindHomography", obj.minMatchesToFindHomography);
obj.info()->addParam(obj, "ransacOutliersRemovalEnabled", obj.ransacOutliersRemovalEnabled);
obj.info()->addParam(obj, "patternSize", obj.patternSize));

CV_INIT_ALGORITHM(ORBDrecognizer, "ObjectRecognition.ORBD",
	obj.info()->addParam(obj, "minKeypointsPerObject", obj.minKeypointsPerObject);
obj.info()->addParam(obj, "distanceRatioThreshold", obj.distanceRatioThreshold);
obj.info()->addParam(obj, "minRecognitionConfidence", obj.minRecognitionConfidence);
obj.info()->addParam(obj, "minMatchesToFindHomography", obj.minMatchesToFindHomography);
obj.info()->addParam(obj, "ransacOutliersRemovalEnabled", obj.ransacOutliersRemovalEnabled);
obj.info()->addParam(obj, "patternSize", obj.patternSize));

CV_INIT_ALGORITHM(NULLrecognizer, "ObjectRecognition.NULL",
	obj.info()->addParam(obj, "nullParam", obj.nullParam));

/*!
Initializes object recognition module. This function should be called once before creating ObjectRecognizer object.
*/
bool initModule_objectRecognition()
{
	bool all = true;

	all &= !SIFTDrecognizer_info_auto.name().empty();
	all &= !ORBDrecognizer_info_auto.name().empty();

	return all;
}


/*!
Stores information extracted from provided \a colorImage, \a depthImage and image \a mask..
*/
bool ObjectRecognizer::addObject(const cv::Mat &colorImage, const cv::Mat &depthImage, const cv::Mat &mask)
{
	if (colorImage.empty() || depthImage.empty() || mask.empty())
		return false;

	return addObjectImpl(colorImage, depthImage, mask);
}


/*!
Returns index of recognized object in provided \a colorImage, aligned \a depthImage and \a mask. If no object was recognized this method returns negative index.
*/
int ObjectRecognizer::recognizeObject(const cv::Mat &colorImage, const cv::Mat &depthImage, const cv::Mat &mask)
{
	if (colorImage.empty() || depthImage.empty() || mask.empty())
		return -1;

	return recognizeObjectImpl(colorImage, depthImage, mask);
}

int ObjectRecognizer::getNumberOfObjects()
{
	return getNumberOfObj();
}

/*!
Returns true if object recognizer object was successfully initialized.
*/
bool ObjectRecognizer::isInitialized() const
{
	return true;
}

/*!
Initialize the object recognizer and possible depth descriptor with additional parameters
*/
void ObjectRecognizer::initialize(int cWidth, int cHeight)
{
	initializeParameters(cWidth, cHeight);
}

/*!
Creates new instance of object recognizer subclass of specified \a recognizerType.
*/
cv::Ptr<ObjectRecognizer> ObjectRecognizer::create(const std::string &recognizerType)
{
	ObjectRecognizer *recognizer = NULL;

	if (!recognizerType.compare("SIFTD")) {
		recognizer = new SIFTDrecognizer(true);
	}
	else if (!recognizerType.compare("ORBD")) {
		recognizer = new ORBDrecognizer(true);
	}
	else if (!recognizerType.compare("SIFT")) {
		recognizer = new SIFTDrecognizer(false);
	}
	else if (!recognizerType.compare("ORB")) {
		recognizer = new ORBDrecognizer(false);
	}
	else{
		recognizer = new NULLrecognizer();
	}

	if (recognizer != NULL && !recognizer->isInitialized()) {
		delete recognizer;
		recognizer = NULL;
	}
	return recognizer;
}


/*!
Creates new instance of object recognizer subclass of specified \a recognizerType and initializes it with provided \a featureDetector, \a descriptorExtractor, \a descriptorMatcher and \a siftGpu.
*/
cv::Ptr<ObjectRecognizer> ObjectRecognizer::create(const std::string &recognizerType, cv::Ptr<cv::FeatureDetector> featureDetector, cv::Ptr<cv::DescriptorExtractor> descriptorExtractor, cv::Ptr<cv::DescriptorMatcher> descriptorMatcher, kcv::KCV_sensor *kcv_sensor, cv::Ptr<SiftGPU> siftGpu)
{
	ObjectRecognizer *recognizer = NULL;

	if (!recognizerType.compare("SIFTD")) {
		recognizer = new SIFTDrecognizer(featureDetector, descriptorExtractor, descriptorMatcher, kcv_sensor, siftGpu);
	}
	if (!recognizerType.compare("ORBD")) {
		recognizer = new ORBDrecognizer();
	}


	if (recognizer != NULL && !recognizer->isInitialized()) {
		delete recognizer;
		recognizer = NULL;
	}
	return recognizer;
}
